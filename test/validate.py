#!/usr/bin/env python3
# coding=utf-8
#
# validate multiple JSON files:
#
#   ./validate.py -s observation_message_schema observation_message.json
#   ./validate.py -s forecast_message_schema forecast_message.json
#   ./validate.py -s texts_message_schema texts_message.json

import argparse
import json

import jsonschema

parser = argparse.ArgumentParser()
parser.add_argument("filename", metavar="filename", help="JSON filename with extension")
parser.add_argument("-s", dest="schema", help="JSON schema w/o extension")
args = parser.parse_args()
with open(f"{args.schema}.json", "r") as schema_file:
    schema = json.load(schema_file)

with open(args.filename, "r") as json_file:
    data = json.load(json_file)

jsonschema.validate(data, schema)
