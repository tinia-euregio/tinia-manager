#!/usr/bin/env python3
# coding=utf-8
#
# compare the schemas in the repo with those published in the manager API

import json

import requests

schemas = [
    "forecast_record_schema",
    "forecast_message_schema",
    "observation_record_schema",
    "observation_message_schema",
    "texts_record_schema",
    "texts_message_schema",
]
base = "http://localhost:8000"
# base = "https://tinia-manager.simevo.com"


for schema in schemas:
    print(f"== comparing {schema}")
    with open(f"test/{schema}.json", "r") as schema_file:
        schema1 = json.load(schema_file)
        # print(json.dumps(schema1, indent=2, sort_keys=True))

    url = f"{base}/api/schemas/{schema}"
    with requests.get(url) as r:
        r.raise_for_status()
        schema2 = json.loads(r.content)
        # print(json.dumps(schema2, indent=2, sort_keys=True))

    assert schema1 == schema2
