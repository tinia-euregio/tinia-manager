# meteo.report Manager

Back-office for the [Euregio Weather Report public-facing website](https://gitlab.com/tinia-euregio/tinia-website).

## Project structure

- **documentation** content is in `docs/` dir (mixed [reStructuredText](https://docutils.sourceforge.io/rst.html) and [Markdown](https://daringfireball.net/projects/markdown/) formats)

- `compose/` and `.dockerignore`: **docker** setup

- `local.yml` and `production.yml`: **docker-compose** files for local development and deployment

- `requirements/`: specifies the 3rd-party Python libraries used and their versions

- `config/`, `setup.cfg` and `.envs/`: **configuration** files

- `manage.py`: the main **entry point** to start, stop and control the apps

- `tinia_manager/`: where the Python source code for the apps resides

## Getting Started

If you want to use meteo.report Manager, read the **user docs**:

- [System administration guide](docs/administration.md)

If you want to hack on this code, read the **developer docs**:

- [Developer Guide](docs/development.md)

- [Data Models](docs/data_models.md) (backgrounder on the database structure)

If you are a sysadmin, read the **admin docs**:

- [Deployment Guide](docs/deployment.md) to install it in production.

## Description

**meteo.report Manager** is a dynamic server-side-rendered web application, for use by meteorologists and website managers:

- designed to update the content and structure of the [Euregio Weather Report public-facing website](https://gitlab.com/tinia-euregio/tinia-website) both interactively and programmatively via **web-hooks**

- optimized for use from desktop or laptop computers

- only available in English

- with different authorization mechanisms for the user interface (access via username / password) and for the web-hooks (API keys)

## Features

### Interactive operation

After logging in to the web-app the users can:

1. see an overview of what texts and data was uploaded, i.e. two lists of bulletins in order of decreasing `last_update` showing:

    - user-friendly view of the start / end / last_update timestamps
    - author
    - version
    - status (draft / non-draft)
    - user-friendly name of the region (`id_venue`)

    there will be two separate lists (a bulletin may appear in both lists):

    - list of bulletins that have associated `TEXTS` records, additional info:

        - count of non-null associated text fields in each language
        - the "currently active" bulletin for each region (i.e. the lastest bulletin for the lastest `start/end` time range) is highlighed

    - list of bulletins that have associated `DATA` records, additional info:

        - count of associated data fields
        - count of keys in each of the `value` fields (the `value` fields of all `data` records with the same `id_data_bulletin` have the same keys)

2. manage the weather forecast texts for next 6 days for all the 4 regions and the 3 mountain regions in the three languages supported by the [Euregio Weather Report public-facing website](https://gitlab.com/tinia-euregio/tinia-website): English, German and Italian:

    - create a new text bulletin for one of the 4 regions with the start date set to today and the draft flag to `True`; a text bulletin for region X has texts in three languages for all venues whose `region_id` is X; the "create new bulletin" command takes the most recent non-draft bulletin for the same region, initializing ("_first guess_") all texts to the values in the previous bulletin, offsetting them if necessary (i.e. if the most recent non-draft bulletin for the same region is yesteday's, the texts for today will be set to yesterday's bulletin texts for the 2nd day, the texts for the 2nd day will be set to yesterday's bulletin texts for the 3nd day etc. and the last day's textx will be set to NULL)

    - non-draft bulletins:

        - show in read-only mode
        - clone an old bulletin with its associated `TEXTS` records to a new `draft` one
        - restore the texts for a certain region to a past "state", i.e. the whole set of textual data before an update (this will be useful if, for example, an incorrect text has been inserted by mistake) by "touching" the previously sent bulletin (`UPDATE bulletins SET last_update = NOW() WHERE ...`).

    - draft bulletins:

        - edit texts and save the bulletin incrementing the `version` and updating `author` and `last_update` fields
        - "send" a draft bulletin, setting the draft field to `True` and thereby making it public and read-only

3. edit articles (containing hyperlinks) in [Markdown format](https://daringfireball.net/projects/markdown/), upload images and embed external videos for the blog and news pages in the three languages supported by the [Euregio Weather Report public-facing website](https://gitlab.com/tinia-euregio/tinia-website): English, German and Italian.

### Programmative operation

Web-hooks authenticated via API keys can be used to:

- upload observation and forecast numeric data in JSON format; bulk uploads 

- upload raster image files for models and radar

- upload webcam image and video files

All web-hooks perform data validation using JSON schema / image sanitizers, immediately notifying the data provider whether the data is acceptable (HTTP status code 200) or rejected (status codes 40x); on successful data receipt, they trigger asynchronous jobs to:

- postprocess data or images

- command the continuous delivery machinery to recompile the statically generated site

- transfer files via rsync to the public-facing website server.

### Logging and monitoring

- includes an email notification system to alert managers if critical errors occur, e.g., if data is missing or one or more of the "required" texts have not been published by the set 
deadline **TO BE REVIEWED**

## User types and roles

meteo.report Manager differentiates between superusers / staff users and plain users:

- superusers / staff users can:

  - edit the configuration and user data
  - deactivate entire sections of the site (e.g. the radar observations or the webcams)
  - deactivate parts of pages (e.g. the text in the local forecast page) **TO BE REVIEWED**
  - hide individual elements, such as a single webcam or weather station 

- plain users are associated to one or more regions (Euregio / Tirol / Südtirol / Trentino) based on the weather service affiliation 

- a user affiliated with a certain weather service can only modify data "belonging" to that service - the other sections or fields will be read-only and grayed-out

## License

Copyright (C) 2022-2024 [simevo s.r.l.](https://simevo.com) for [Europaregion Tirol/Südtirol/Trentino](https://www.europaregion.info).

Licensed under the [GNU Affero General Public License version 3 or later (`AGPL-3.0-or-later`)](LICENSE) as per [linee guida per l’Acquisizione e il riuso di software per la Pubblica Amministrazione](https://docs.italia.it/italia/developers-italia/lg-acquisizione-e-riuso-software-per-pa-docs/it/stabile/riuso-software/licenze-aperte-e-scelta-di-una-licenza.html#scelta-di-una-licenza).
