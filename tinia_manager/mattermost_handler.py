from logging import Handler

import environ
import requests

env = environ.Env()


class MattermostHandler(Handler):
    def __init__(self, base_url, access_token, channel_id):
        super().__init__()
        self.url = base_url + "/api/v4/posts"
        self.access_token = access_token
        self.channel_id = channel_id

    def emit(self, record):
        message = "Error from " + env("SERVER_ADDRESS") + ":\n"
        message += "```\n" + self.format(record) + "\n```"

        requests.post(
            self.url,
            headers={
                "Authorization": f"Bearer {self.access_token}",
                "Content-Type": "application/json",
            },
            json={
                "channel_id": self.channel_id,
                "message": message,
            },
        )
