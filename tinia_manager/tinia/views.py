import base64
import datetime
import json
import math
import tempfile
import time
import zipfile

import jsonschema
import netCDF4
import requests
from django.db.models import Count
from django.db.transaction import on_commit
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView, View
from django_filters import rest_framework as filters
from rest_framework.decorators import api_view, permission_classes
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import SAFE_METHODS, BasePermission
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework_api_key.permissions import HasAPIKey

from config.settings.base import env
from tinia_manager.tinia import tasks
from tinia_manager.tinia.models import (
    CurrentData,
    CurrentTexts,
    Data,
    DataBulletin,
    DataType,
    PseudoRadarBulletin,
    SkyCondition,
    Text,
    TextBulletin,
    TimeLayout,
    Venue,
    VenueType,
    Webcam,
)
from tinia_manager.tinia.serializers import (
    CurrentDataViewSerializer,
    CurrentTextsViewSerializer,
    DataBulletinSerializer,
    DataSerializer,
    DataTypeSerializer,
    PseudoRadarBulletinSerializer,
    SkyConditionSerializer,
    TimeLayoutSerializer,
    VenueSerializer,
    VenueTypeSerializer,
    WebcamSerializerDynamic,
)
from tinia_manager.users.models import User

date_format = "%Y-%m-%dT%H:%M:%S"
date_format_human = "%A, %d-%m-%Y, %H:%M, %Z"

current_tz = timezone.get_current_timezone()


def get_today_midnight():
    return datetime.datetime.combine(datetime.date.today(), datetime.time.min, datetime.timezone.utc)


def format_timestamp(value):
    return timezone.make_naive(value, current_tz).strftime(date_format)


def format_timestamp_human(value):
    return value.astimezone(current_tz).strftime(date_format_human)


def format_timestamp_human_utc(value):
    return value.astimezone(timezone.utc).strftime(date_format_human)


class AboutPageView(TemplateView):
    template_name = "pages/about.html"

    def get(self, request):
        return render(
            request,
            "pages/about.html",
        )


class IndexPageView(TemplateView):
    template_name = "pages/bulletin_texts.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            return render(request, "pages/bulletin_texts.html")


def home(request):
    # number of each page for pagination
    num = 10

    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    else:
        # get sort data from frontend
        month = request.GET.get("month")
        year = request.GET.get("year")
        page = int(request.GET.get("page"))
        sort_col = request.GET.get("sortCol")
        sort_asc = request.GET.get("sortAsc")
        is_data_page = request.GET.get("isDataPage")
        is_draft = request.GET.get("isDraft")
        region = request.GET.get("region")

        # filter by month, year, region, is_draft
        # check bulletin for data
        if is_data_page == "1":
            bulletins = DataBulletin.objects.all()
        else:
            bulletins = TextBulletin.objects.all()
        if year:
            bulletins = bulletins.filter(last_update__year=year)
        if month != "All":
            bulletins = bulletins.filter(last_update__month=month)
        if region != "All":
            bulletins = bulletins.filter(id_venue__name_eng=region)

        if is_draft != "All":
            bulletins = bulletins.filter(draft=(is_draft == "true"))

        # get total page number
        if int(bulletins.count() / num) == (bulletins.count() / num):
            total_pages = int(bulletins.count() / num)
        else:
            total_pages = int(bulletins.count() / num) + 1
        if sort_col == "null":
            sort_col = "-last_update"
        else:
            if sort_asc == "true":
                sort_col = "-" + sort_col

        if sort_col != "data_num" and sort_col != "-data_num":
            bulletins = bulletins.order_by(sort_col)[num * (page - 1) : num * page]
        else:
            if sort_asc == "false":
                bulletins = bulletins.annotate(count=Count("datas__id")).order_by("count")[
                    num * (page - 1) : num * page
                ]
            else:
                bulletins = bulletins.annotate(count=Count("datas__id")).order_by("-count")[
                    num * (page - 1) : num * page
                ]

        editable_statuses = []
        for requested_bulletin in bulletins:
            regioncheck = 0
            if requested_bulletin.id_venue in request.user.region.all():
                regioncheck = 1
            if request.user.is_superuser or requested_bulletin.draft and regioncheck == 1:
                editable_status = 1
            else:
                editable_status = 0
            editable_statuses.append(editable_status)
        bulletins = list(bulletins.values())

        for i, bulletin in enumerate(bulletins):
            bulletin["editable_status"] = editable_statuses[i]
            bulletin["region"] = Venue.objects.get(id=bulletin["id_venue_id"]).name_eng
            bulletin["start_human"] = format_timestamp_human(bulletin["start"])
            bulletin["start_human_utc"] = format_timestamp_human_utc(bulletin["start"])
            bulletin["end_human"] = format_timestamp_human(bulletin["end"])
            bulletin["end_human_utc"] = format_timestamp_human_utc(bulletin["end"])
            bulletin["last_update_human"] = format_timestamp_human(bulletin["last_update"])
            bulletin["last_update_human_utc"] = format_timestamp_human_utc(bulletin["last_update"])
            if is_data_page == "1":
                bulletin["data_num"] = Data.objects.filter(id_data_bulletin=bulletin["id"]).count()
                bulletin["number_eng"] = 0
                bulletin["number_deu"] = 0
                bulletin["number_ita"] = 0
                bulletin["number_lld"] = 0
            else:
                bulletin_texts = TextBulletin.objects.get(id=bulletin["id"]).texts
                bulletin_one_day_texts = (
                    bulletin_texts.filter(id_time_layout=TimeLayout.get_start_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_second_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_third_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_fourth_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_fifth_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day())
                    | bulletin_texts.filter(id_time_layout=TimeLayout.get_mountain_day())
                )
                number_eng = number_deu = number_ita = number_lld = 0
                for bulletin_one_day_text in bulletin_one_day_texts:
                    if bulletin_one_day_text.value_eng != "":
                        number_eng += 1
                    if bulletin_one_day_text.value_deu != "":
                        number_deu += 1
                    if bulletin_one_day_text.value_ita != "":
                        number_ita += 1
                    if bulletin_one_day_text.value_lld != "":
                        number_lld += 1
                bulletin["number_eng"] = number_eng
                bulletin["number_deu"] = number_deu
                bulletin["number_ita"] = number_ita
                bulletin["number_lld"] = number_lld
                bulletin["data_num"] = 0

        regions = []
        for item in request.user.region.all():
            region = {"id": item.id, "name": item.name_eng}
            regions.append(region)

        main_regions = ["Euregio", "Tyrol", "South Tyrol", "Trentino"]
        active_ids = []
        for main_region in main_regions:
            active_bulletin = (
                TextBulletin.objects.filter(id_venue__name_eng=main_region, draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-start", "-last_update")
            )
            if active_bulletin:
                active_ids.append(active_bulletin.first().id)
        lastupdate_ids = []
        td = datetime.datetime.combine(datetime.date.today(), datetime.datetime.min.time())
        for main_region in main_regions:
            last_edit = TextBulletin.objects.filter(
                id_venue__name_eng=main_region, draft=True, start__date=td.date()
            ).order_by("-last_update")
            if last_edit:
                lastupdate_ids.append(last_edit.first().id)
        data = {
            "bulletins": bulletins,
            "regions": regions,
            "totalpages": total_pages,
            "active_ids": active_ids,
            "lastupdate_ids": lastupdate_ids,
        }
    return JsonResponse(data, safe=False)


def bulletinTextCreateTimelayoutReturn(time_layout, day_offset):
    time_layout_mountain = TimeLayout.get_mountain_day()
    if time_layout_mountain.id == time_layout:
        return time_layout_mountain.id
    else:
        time_layout_first = TimeLayout.get_start_day()
        time_layout_second = TimeLayout.get_second_day()
        time_layout_third = TimeLayout.get_third_day()
        time_layout_fourth = TimeLayout.get_fourth_day()
        time_layout_fifth = TimeLayout.get_fifth_day()
        time_layout_last = TimeLayout.get_last_day()
        time_layouts = [
            time_layout_first.id,
            time_layout_second.id,
            time_layout_third.id,
            time_layout_fourth.id,
            time_layout_fifth.id,
            time_layout_last.id,
        ]

        time_layout_lookup = {
            time_layout_first.id: 0,
            time_layout_second.id: 1,
            time_layout_third.id: 2,
            time_layout_fourth.id: 3,
            time_layout_fifth.id: 4,
            time_layout_last.id: 5,
        }
        index = time_layout_lookup[time_layout] - day_offset
        return time_layouts[index]


class BulletinCreate(TemplateView):
    template_name = "pages/bulletinview.html"

    def post(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            region_id = request.POST["region"]
            mountain_region_id = Venue.objects.all().get(id_region_id=region_id, id_venue_type_id=1).id
            td = get_today_midnight()
            start = format_timestamp(td)
            yd = td - datetime.timedelta(days=1)
            yyd = td - datetime.timedelta(days=2)
            yyyd = td - datetime.timedelta(days=3)
            yyyyd = td - datetime.timedelta(days=4)
            yyyyyd = td - datetime.timedelta(days=5)
            end = datetime.datetime.combine(
                datetime.date.today(), datetime.time.max, datetime.timezone.utc
            ) + datetime.timedelta(days=5)
            latest_t_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=td.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_t_bulletin =', latest_t_bulletin)
            latest_y_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=yd.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_y_bulletin =', latest_y_bulletin)
            latest_yy_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=yyd.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_yy_bulletin =', latest_yy_bulletin)
            latest_yyy_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=yyyd.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_yyy_bulletin =', latest_yyy_bulletin)
            latest_yyyy_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=yyyyd.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_yyyy_bulletin =', latest_yyyy_bulletin)
            latest_yyyyy_bulletin = (
                TextBulletin.objects.filter(id_venue_id=region_id, start__date=yyyyyd.date(), draft=False)
                .annotate(texts_count=Count("texts"))
                .filter(texts_count__gt=0)
                .order_by("-last_update")
                .first()
            )
            # print('========== latest_yyyyy_bulletin =', latest_yyyyy_bulletin)
            newbulletin = TextBulletin.objects.create(
                id_venue_id=region_id,
                reference=start,
                start=start,
                end=end,
                last_update=timezone.make_aware(datetime.datetime.now()),
                author=request.user.username,
                version=0,
                draft=True,
            )
            if (
                latest_t_bulletin is not None
                or latest_y_bulletin is not None
                or latest_yy_bulletin is not None
                or latest_yyy_bulletin is not None
                or latest_yyyy_bulletin is not None
                or latest_yyyyy_bulletin is not None
            ):
                if latest_t_bulletin is not None:
                    bulletin_texts = latest_t_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_start_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_second_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_second_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_third_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_third_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_fourth_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fourth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_fifth_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_fifth_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_end_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_mountain = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_mountain_day()
                    ).filter(id_venue=mountain_region_id)
                    # TIME LAYOUT TEMPORANEO
                    bulletin_new_texts = (
                        bulletin_texts_start_day
                        | bulletin_texts_second_day
                        | bulletin_texts_third_day
                        | bulletin_texts_fourth_day
                        | bulletin_texts_fifth_day
                        | bulletin_texts_end_day
                        | bulletin_texts_mountain
                    )
                    for bulletin_new_text in bulletin_new_texts:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletin_new_text.id_time_layout_id,
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                elif latest_y_bulletin is not None:
                    bulletin_texts = latest_y_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_second_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_second_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_third_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_third_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fourth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_fourth_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fifth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_fifth_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_mountain = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_mountain_day()
                    ).filter(id_venue=mountain_region_id)
                    # TIME LAYOUT TEMPORANEO
                    bulletin_new_texts = (
                        bulletin_texts_start_day
                        | bulletin_texts_second_day
                        | bulletin_texts_third_day
                        | bulletin_texts_fourth_day
                        | bulletin_texts_fifth_day
                        | bulletin_texts_mountain
                    )
                    for bulletin_new_text in bulletin_new_texts:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletinTextCreateTimelayoutReturn(
                                bulletin_new_text.id_time_layout_id, 1
                            ),
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_last_day())
                elif latest_yy_bulletin is not None:
                    bulletin_texts = latest_yy_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_third_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_second_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fourth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_third_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_fifth_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_fourth_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_mountain = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_mountain_day()
                    ).filter(id_venue=mountain_region_id)
                    # TIME LAYOUT TEMPORANEO
                    bulletin_new_texts = (
                        bulletin_texts_start_day
                        | bulletin_texts_second_day
                        | bulletin_texts_third_day
                        | bulletin_texts_fourth_day
                        | bulletin_texts_mountain
                    )
                    for bulletin_new_text in bulletin_new_texts:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletinTextCreateTimelayoutReturn(
                                bulletin_new_text.id_time_layout_id, 2
                            ),
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_fifth_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_last_day())
                elif latest_yyy_bulletin is not None:
                    bulletin_texts = latest_yyy_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fourth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_second_day = bulletin_texts.filter(
                        id_time_layout=TimeLayout.get_fifth_day()
                    ).exclude(id_venue=mountain_region_id)
                    bulletin_texts_third_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )

                    bulletin_new_texts = bulletin_texts_start_day | bulletin_texts_second_day | bulletin_texts_third_day
                    for bulletin_new_text in bulletin_new_texts:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletinTextCreateTimelayoutReturn(
                                bulletin_new_text.id_time_layout_id, 3
                            ),
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_fourth_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_fifth_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_last_day())
                    Text.create_new_text(newbulletin, mountain_region_id, TimeLayout.get_mountain_day())
                elif latest_yyyy_bulletin is not None:
                    bulletin_texts = latest_yyyy_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_fifth_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_texts_second_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    bulletin_new_texts = bulletin_texts_start_day | bulletin_texts_second_day
                    for bulletin_new_text in bulletin_new_texts:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletinTextCreateTimelayoutReturn(
                                bulletin_new_text.id_time_layout_id, 4
                            ),
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_third_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_fourth_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_fifth_day())
                    Text.create_new_text(newbulletin, region_id, TimeLayout.get_last_day())
                    Text.create_new_text(newbulletin, mountain_region_id, TimeLayout.get_mountain_day())

                elif latest_yyyyy_bulletin is not None:
                    bulletin_texts = latest_yyyyy_bulletin.texts.order_by("id")
                    bulletin_texts_start_day = bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day()).exclude(
                        id_venue=mountain_region_id
                    )
                    for bulletin_new_text in bulletin_texts_start_day:
                        Text.objects.create(
                            id_text_bulletin=newbulletin,
                            id_venue_id=bulletin_new_text.id_venue_id,
                            id_time_layout_id=bulletinTextCreateTimelayoutReturn(
                                bulletin_new_text.id_time_layout_id, 5
                            ),
                            value_eng=bulletin_new_text.value_eng,
                            value_deu=bulletin_new_text.value_deu,
                            value_ita=bulletin_new_text.value_ita,
                            value_lld=bulletin_new_text.value_lld,
                        )
                current_bulletin = TextBulletin.objects.get(id=newbulletin.id)
                current_bulletin_texts = current_bulletin.texts.order_by("id")
                bulletin_texts_start_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_start_day())
                bulletin_texts_second_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_second_day())
                bulletin_texts_third_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_third_day())
                bulletin_texts_fourth_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_fourth_day())
                bulletin_texts_fifth_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_fifth_day())
                bulletin_texts_end_day = current_bulletin_texts.filter(id_time_layout=TimeLayout.get_last_day())
                bulletin_texts_mountain = current_bulletin_texts.filter(
                    id_time_layout=TimeLayout.get_mountain_day()
                ).filter(id_venue=mountain_region_id)
            else:
                bulletin_texts_start_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_start_day())}
                bulletin_texts_second_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_second_day())}
                bulletin_texts_third_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_third_day())}
                bulletin_texts_fourth_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_fourth_day())}
                bulletin_texts_fifth_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_fifth_day())}
                bulletin_texts_end_day = {Text.create_new_text(newbulletin, region_id, TimeLayout.get_last_day())}
                bulletin_texts_mountain = {
                    Text.create_new_text(newbulletin, mountain_region_id, TimeLayout.get_mountain_day())
                }

            return HttpResponseRedirect(reverse("bulletin_view", kwargs={"pk": str(newbulletin.id)}))


def sendView(request):
    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    else:
        current_bulletin_id = request.GET["current_bulletin_id"]
        current_bulletin = TextBulletin.objects.get(id=current_bulletin_id)
        current_bulletin.draft = False
        current_bulletin.last_update = timezone.make_aware(datetime.datetime.now())
        current_bulletin.save()
        on_commit(lambda: tasks.post_process_text_bulletin.delay(current_bulletin_id))
        return HttpResponse("")


def bulletinTextUpdate(request):
    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    else:
        bulletinid = request.POST["bulletin_id"]
        requested_bulletin = TextBulletin.objects.get(id=bulletinid)
        requested_bulletin.last_update = timezone.make_aware(datetime.datetime.now())
        requested_bulletin.author = request.user.username
        requested_bulletin.version = requested_bulletin.version + 1
        requested_bulletin.save()
        bulletin_texts = requested_bulletin.texts.order_by("id_time_layout__id")
        for daydata in bulletin_texts:
            print("========== daydata =", daydata.__dict__)
            combname = (
                "_"
                + str(daydata.id)
                + "_"
                + str(daydata.id_text_bulletin_id)
                + "_"
                + str(daydata.id_venue_id)
                + "_"
                + str(daydata.id_time_layout_id)
            )
            print("========== request.POST =", request.POST)
            value_eng = request.POST["eng" + combname]
            value_deu = request.POST["deu" + combname]
            value_ita = request.POST["ita" + combname]
            value_lld = request.POST["lld" + combname]
            daydatatext = Text.objects.get(id=daydata.id)
            daydatatext.value_eng = value_eng
            daydatatext.value_deu = value_deu
            daydatatext.value_ita = value_ita
            daydatatext.value_lld = value_lld
            daydatatext.save()
        return HttpResponseRedirect(reverse("bulletin_view", kwargs={"pk": str(bulletinid)}))


def bulletinDelete(request, pk):
    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    else:
        app_id = pk
        requested_bulletin = TextBulletin.objects.get(id=app_id)
        if request.user.is_authenticated and (
            request.user.is_superuser
            or request.user.username == requested_bulletin.author
            and requested_bulletin.draft is True
        ):
            Text.objects.filter(id_text_bulletin=requested_bulletin).delete()
            requested_bulletin.delete()
            return HttpResponseRedirect(reverse("bulletin_texts"))
        else:
            return render(request, "pages/access_denied.html")


class BulletinDetailView(TemplateView):
    def get(self, request, pk):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            app_id = pk
            regioncheck = 0
            requested_bulletin = TextBulletin.objects.get(id=app_id)
            second_day = requested_bulletin.start + datetime.timedelta(days=1)
            third_day = requested_bulletin.start + datetime.timedelta(days=2)
            fourth_day = requested_bulletin.start + datetime.timedelta(days=3)
            fifth_day = requested_bulletin.start + datetime.timedelta(days=4)
            sixth_day = requested_bulletin.start + datetime.timedelta(days=5)
            bulletin_texts = requested_bulletin.texts.order_by("id_time_layout__id")
            for user_region in request.user.region.all():
                if user_region == requested_bulletin.id_venue:
                    regioncheck = 1
            if request.user.is_superuser or requested_bulletin.draft and regioncheck == 1:
                editable_status = 1
            else:
                editable_status = 0
            data = {
                "bulletin_texts": bulletin_texts,
                "bulletin": requested_bulletin,
                "second_day": second_day,
                "third_day": third_day,
                "fourth_day": fourth_day,
                "fifth_day": fifth_day,
                "sixth_day": sixth_day,
                "editable_status": editable_status,
            }
            return render(request, "pages/bulletinview.html", data)


def secret(path):
    if secret.cached.get(path, "") == "":
        with open(path, "r") as secret_file:
            encoded_secret = secret_file.read()
        secret.cached[path] = encoded_secret
    return secret.cached[path]


secret.cached = {}


class TranslationFailure(Exception):
    pass


def translation(request):
    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    else:
        from_lang = request.GET.get("from")
        fromLangCode = from_lang[:-1]
        to_lang = request.GET.get("to")
        base_language = to_lang[:-1]
        userpass = secret("secrets/userpass")
        content = request.GET.get("text")
        headers = {
            "Authorization": "Basic " + base64.b64encode(userpass.encode()).decode(),
        }
        print(headers)
        data = {
            "source": fromLangCode,
            "target": base_language,
            "q": content,
        }
        print(data)
        fbk_ip = secret("secrets/fbk_ip")
        url = f"https://{fbk_ip}/translate/"
        translationData = requests.get(url, headers=headers, data=data, verify=False)  # make request
        if translationData.status_code == 200:
            print(translationData.text)
            content_translated = translationData.json()["data"]["translation"]
        else:
            raise TranslationFailure(
                "err! status code=%s,status message=%s" % (translationData.status_code, translationData.text)
            )

        data = {"result": content_translated}
        print(data)
        return JsonResponse(data, safe=False)


class BulletinDataDetail(TemplateView):
    template_name = "pages/bulletin_data.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            return render(request, "pages/bulletin_data.html")


class DashboardPageView(TemplateView):
    template_name = "pages/dashboard.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            return render(request, "pages/dashboard.html")


class CmsPageView(TemplateView):
    template_name = "pages/cms.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            return render(request, self.template_name)


@method_decorator(csrf_exempt, name="dispatch")
class GitlabProxyView(View):
    def get(self, request, path):
        url = f"https://gitlab.com/{path}"
        # headers = request.headers
        token = env("DJANGO_GITLAB_TOKEN")
        headers = {
            "Authorization": f"Bearer {token}",
            "Host": "gitlab.com",
            "Content-Type": "application/json; charset=utf-8",
        }
        # patch for Django replacing %2F with /, but in gitlab API NAMESPACE/PROJECT_PATH must be URL-encoded
        # see: https://docs.gitlab.com/ee/api/index.html#namespaced-path-encoding
        url = url.replace("tinia-euregio/", "tinia-euregio%2F")
        # file_path should be a URL encoded full path: https://docs.gitlab.com/ee/api/repository_files.html
        url = url.replace("content/news/", "content%2Fnews%2F")
        url = url.replace("content/blog/", "content%2Fblog%2F")
        url = url.replace("static/img/", "static%2Fimg%2F")
        querystring = request.GET.urlencode()
        if querystring:
            url += f"?{querystring}"
        print(f"proxying GET {url} with headers: {headers} and query parameters {querystring}")
        upstream = requests.get(url, headers=headers)
        response_headers = {
            "Content-Type": upstream.headers["Content-Type"],
        }
        if upstream.status_code == 200:
            return HttpResponse(upstream.content, headers=response_headers)
        else:
            status_code = upstream.status_code
            message = "The request is not valid."
            return JsonResponse({"message": message}, status=status_code)

    def post(self, request, path):
        url = f"https://gitlab.com/{path}"
        token = env("DJANGO_GITLAB_TOKEN")
        headers = {
            "Authorization": f"Bearer {token}",
            "Host": "gitlab.com",
            "Content-Type": "application/json;",
        }
        url = url.replace("tinia-euregio/", "tinia-euregio%2F")
        querystring = request.GET.urlencode()
        if querystring:
            url += f"?{querystring}"
        print(f"proxying POST {url} with headers: {headers} and query parameters {querystring}")
        response_headers = {
            "Content-Type": "application/json;",
        }
        body = request.body
        upstream = requests.post(url, headers=headers, data=body)
        # print(upstream.content)
        return HttpResponse(upstream.content, headers=response_headers)


class ManageRegion(TemplateView):
    template_name = "pages/user_region.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        elif not request.user.is_superuser:
            return HttpResponseRedirect(reverse("bulletin_texts"))
        else:
            users = User.objects.all().order_by("id")
            data = {"users": users}
            return render(request, "pages/user_region.html", data)


class RegionSelect(TemplateView):
    template_name = "pages/region_select.html"

    def post(self, request, id):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        elif not request.user.is_superuser:
            return HttpResponseRedirect(reverse("bulletin_texts"))
        else:
            user = User.objects.get(id=id)
            regions = ["Euregio", "Tyrol", "South Tyrol", "Trentino"]
            user_regions_query = user.region.all()
            user_regions = []
            for user_region_query in user_regions_query:
                user_regions.append(user_region_query.name_eng)
            data = {"user": user, "regions": regions, "user_regions": user_regions}
            return render(request, "pages/region_select.html", data)


def userRegionSave(request):
    if not request.user.is_authenticated:
        current_url = request.get_full_path()
        return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
    elif not request.user.is_superuser:
        return HttpResponseRedirect(reverse("bulletin_texts"))
    else:
        user_id = request.POST.get("user_id")
        user = User.objects.get(id=user_id)
        regions = ["Euregio", "Tyrol", "South Tyrol", "Trentino"]
        for region in regions:
            if request.POST.get(region, "False") == "True":
                user.region.add(Venue.objects.get(name_eng=region))
            else:
                user.region.remove(Venue.objects.get(name_eng=region))
        return HttpResponseRedirect(reverse("user_region"))


class WebHookException(Exception):
    pass


def generic_webhook(request, schema, BulletinType, strict=True):
    # perform data validation
    if request.content_type != "application/json":
        raise WebHookException("Please send data as application/json")
    if int(request.META["CONTENT_LENGTH"]) == 0:
        raise WebHookException("CONTENT_LENGTH is zero")
    body = request.body.decode("utf-8")
    if len(body) == 0:
        raise WebHookException("body length is zero")
    filename = "/tmp/" + time.strftime("%Y%m%d%H%M%S") + ".json"
    with open(filename, "wt") as f:
        f.write(body)
    try:
        message = json.loads(body)
    except Exception:
        raise WebHookException("unparseable JSON")
    message_schema = DataType.objects.get(pk=schema).schema
    try:
        jsonschema.validate(message, message_schema)
    except Exception:
        raise WebHookException("invalid JSON")
    tls = [value["id_time_layout"] for value in message["data"][0]["values"]]
    if len(tls) != len(set(tls)):
        raise WebHookException("time layouts are not unique")
    if strict:
        # verify data completeness: the data for each venue has the same set of time layouts
        if len(message["data"]) > 1:
            for data in message["data"][1:]:
                tls1 = [value["id_time_layout"] for value in data["values"]]
                if set(tls) != set(tls1):
                    raise WebHookException(f"time layouts for venue {data['id_venue']} are inconsistent")
    # TODO: verify that all venues belong to the region advertised in the metadata!
    key_prefix = request.META["HTTP_AUTHORIZATION"].split()[1].split(".")[0]

    # was: generic_post_process
    min_tl_id = min(message["data"][0]["values"], key=lambda x: x["id_time_layout"])["id_time_layout"]
    max_tl_id = max(message["data"][0]["values"], key=lambda x: x["id_time_layout"])["id_time_layout"]
    is_draft = message.get("draft", False)
    min_tl = TimeLayout.objects.filter(pk=min_tl_id).first()
    if min_tl is None:
        raise WebHookException(f"time layout {min_tl_id} not found")
    max_tl = TimeLayout.objects.filter(pk=max_tl_id).first()
    if max_tl is None:
        raise WebHookException(f"time layout {max_tl_id} not found")
    reference = datetime.datetime.strptime(message["start"], "%Y-%m-%dT%H:%M:%S%z")
    start = reference + datetime.timedelta(
        days=min_tl.start_day_offset,
        hours=min_tl.start_time.hour,
        minutes=min_tl.start_time.minute,
        seconds=min_tl.start_time.second,
    )
    end = reference + datetime.timedelta(
        days=max_tl.end_day_offset,
        hours=max_tl.end_time.hour,
        minutes=max_tl.end_time.minute,
        seconds=max_tl.end_time.second,
    )
    id_venue = message["id_venue"]
    venue = Venue.objects.filter(pk=id_venue).first()
    if venue is None:
        raise WebHookException(f"venue {id_venue} not found")
    new = BulletinType(
        id_venue=venue,
        reference=reference,
        start=start,
        end=end,
        last_update=datetime.datetime.now().astimezone(current_tz),
        author=key_prefix,
        version=0,
        draft=is_draft,
    )
    new.save()
    return (new, message, filename)


@csrf_exempt
@api_view(["POST"])
@permission_classes([HasAPIKey])
def forecasts_webhook(request):
    try:
        (new, message, fname) = generic_webhook(request, "forecast_message_schema", DataBulletin)
    except WebHookException as e:
        return HttpResponseBadRequest(e)
    # data bulletins that carry 24-hours forecasts must have a reference timestamp which is at midnight UTC
    utc_tz = timezone.utc
    if new.reference.astimezone(utc_tz).time() != datetime.time(0, 0):
        raise WebHookException("reference timestamp must be at midnight UTC")

    inserted_records = 1  # generic_webhook has already inserted one record in table bulletins

    venue_type_town = VenueType.objects.get(pk=2)
    venue_type_mountain_subregion_elevation = VenueType.objects.get(pk=5)

    forecast_data_type = DataType.objects.get(pk="forecast_record_schema")
    for data in message["data"]:
        id_venue = data["id_venue"]
        venue = Venue.objects.filter(pk=id_venue).first()
        if venue is None:
            return HttpResponseBadRequest(f"venue {id_venue} not found")
        if venue.id_venue_type != venue_type_town and venue.id_venue_type != venue_type_mountain_subregion_elevation:
            return HttpResponseBadRequest(f"venue {id_venue} is not a town nor a mountain-subregion-elevation")
        for value in data["values"]:
            id_time_layout = value.pop("id_time_layout", None)
            tl = TimeLayout.objects.filter(pk=id_time_layout).first()
            if tl is None:
                return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
            new_data = Data(
                id_data_bulletin=new,
                id_venue=venue,
                id_time_layout=tl,
                id_data_type=forecast_data_type,
                value=value,
            )
            new_data.save()
            inserted_records += 1

    # trigger asynchronous post-processing, adding a job in the Celery task queue
    on_commit(lambda: tasks.post_process_forecast_bulletin.delay(new.id, fname))
    content = {
        "status": "forecast_message_schema data scheduled for post-processing",
        "inserted_records": inserted_records,
    }
    return Response(content)


@csrf_exempt
@api_view(["POST"])
@permission_classes([HasAPIKey])
def observations_webhook(request):
    try:
        (new, message, fname) = generic_webhook(request, "observation_message_schema", DataBulletin)
    except WebHookException as e:
        return HttpResponseBadRequest(e)
    # data bulletins that carry observations (i.e. half-hour data) must have reference timestamps at 00:00, 00:30, 01:00, 01:30 ... 23:30 UTC
    utc_tz = timezone.utc
    reference_time_utc = new.reference.astimezone(utc_tz).time()
    if reference_time_utc.minute % 30 != 0 or reference_time_utc.second != 0:
        raise WebHookException("reference timestamp must be at XX:00 or XX:30 UTC")
    inserted_records = 1  # generic_webhook has already inserted one record in table bulletins

    venue_type_station = VenueType.objects.get(pk=3)

    observation_data_type = DataType.objects.get(pk="observation_record_schema")
    for data in message["data"]:
        id_venue = data["id_venue"]
        venue = Venue.objects.filter(pk=id_venue).first()
        if venue is None:
            return HttpResponseBadRequest(f"venue {id_venue} not found")
        if venue.id_venue_type != venue_type_station:
            return HttpResponseBadRequest(f"venue {id_venue} is not a station")
        for value in data["values"]:
            id_time_layout = value.pop("id_time_layout", None)
            tl = TimeLayout.objects.filter(pk=id_time_layout).first()
            if tl is None:
                return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
            new_data = Data(
                id_data_bulletin=new,
                id_venue=venue,
                id_time_layout=tl,
                id_data_type=observation_data_type,
                value=value,
            )
            new_data.save()
            inserted_records += 1

    # trigger asynchronous post-processing, adding a job in the Celery task queue
    on_commit(lambda: tasks.post_process_observations.delay(new.id, fname))
    content = {
        "status": "observation_message_schema data scheduled for post-processing",
        "inserted_records": inserted_records,
    }
    return Response(content)


@csrf_exempt
@api_view(["POST"])
@permission_classes([HasAPIKey])
def texts_webhook(request):
    try:
        (new, message, fname) = generic_webhook(
            request,
            "texts_message_schema",
            TextBulletin,
            False,
        )
    except WebHookException as e:
        return HttpResponseBadRequest(e)
    # textual bulletins must have a reference timestamp which is at midnight UTC
    utc_tz = timezone.utc
    if new.reference.astimezone(utc_tz).time() != datetime.time(0, 0):
        raise WebHookException("reference timestamp must be at midnight UTC")
    inserted_records = 1  # generic_webhook has already inserted one record in table bulletins
    required_venue_types = [
        0,
        1,
    ]  # the list of venue types that must be present in the bulletin

    for data in message["data"]:
        id_venue = data["id_venue"]
        venue = Venue.objects.filter(pk=id_venue).first()
        if venue is None:
            return HttpResponseBadRequest(f"venue {id_venue} not found")
        required_time_layouts = []
        # print(venue.id_venue_type_id)
        if venue.id_venue_type_id == "0":
            required_time_layouts = [
                144000000,
                144001440,
                144002880,
                144004320,
                144005760,
                144007200,
            ]
            required_venue_types.remove(0)
        elif venue.id_venue_type_id == "1":
            required_time_layouts = [432000000]
            required_venue_types.remove(1)
        for value in data["values"]:
            id_time_layout = value.pop("id_time_layout", None)
            required_time_layouts.remove(id_time_layout)
            tl = TimeLayout.objects.filter(pk=id_time_layout).first()
            if tl is None:
                return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
            new_text = Text(
                id_text_bulletin=new,
                id_venue=venue,
                id_time_layout=tl,
                value_deu=value.get("value_deu", ""),
                value_eng=value.get("value_eng", ""),
                value_ita=value.get("value_ita", ""),
                value_lld=value.get("value_lld", ""),
            )
            new_text.save()
            inserted_records += 1

        for time_layout in required_time_layouts:
            tl = TimeLayout.objects.filter(pk=time_layout).first()
            if tl is None:
                return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
            new_text = Text(
                id_text_bulletin=new,
                id_venue=venue,
                id_time_layout=tl,
                value_deu="",
                value_eng="",
                value_ita="",
                value_lld="",
            )
            new_text.save()

    if 0 in required_venue_types:
        required_time_layouts = [144000000, 144001440, 144002880, 144004320, 144005760]
        for time_layout in required_time_layouts:
            tl = TimeLayout.objects.filter(pk=time_layout).first()
            if tl is None:
                return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
            new_text = Text(
                id_text_bulletin=new,
                id_venue=new.id_venue,
                id_time_layout=tl,
                value_deu="",
                value_eng="",
                value_ita="",
                value_lld="",
            )
            new_text.save()
    if 1 in required_venue_types:
        required_time_layout = 432000000
        tl = TimeLayout.objects.filter(pk=required_time_layout).first()
        if tl is None:
            return HttpResponseBadRequest(f"time layout {id_time_layout} not found")
        venue = Venue.objects.get(id_region=new.id_venue, id_venue_type=1)
        new_text = Text(
            id_text_bulletin=new,
            id_venue=venue,
            id_time_layout=tl,
            value_deu="",
            value_eng="",
            value_ita="",
            value_lld="",
        )
        new_text.save()

    if new.draft:
        content = {
            "status": "draft texts_message_schema data received",
            "inserted_records": inserted_records,
        }
    else:
        # trigger asynchronous post-processing, adding a job in the Celery task queue
        on_commit(lambda: tasks.post_process_text_bulletin.delay(new.id))
        content = {
            "status": "texts_message_schema data scheduled for post-processing",
            "inserted_records": inserted_records,
        }
    return Response(content)


# https://stackoverflow.com/a/56387775
def floor_dt(dt, minutes):
    replace = (dt.minute // minutes) * minutes
    return dt.replace(minute=replace, second=0, microsecond=0)


@csrf_exempt
@api_view(["POST"])
@permission_classes([HasAPIKey])
def pseudoradar_webhook(request):
    if request.content_type != "application/x-netcdf":
        return HttpResponseBadRequest("Please send data as application/x-netcdf")
    if int(request.META["CONTENT_LENGTH"]) == 0:
        return HttpResponseBadRequest("CONTENT_LENGTH is zero")
    if len(request.body) == 0:
        return HttpResponseBadRequest("body length is zero")

    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(request.body)
        f.flush()
        try:
            netCDF4.Dataset(f.name)
        except Exception:
            return HttpResponseBadRequest("NetCDF: Unknown file format")

        key_prefix = request.META["HTTP_AUTHORIZATION"].split()[1].split(".")[0]

        is_draft = False
        last_update = datetime.datetime.now().astimezone(current_tz)
        reference = floor_dt(last_update, 5)
        start = reference - datetime.timedelta(minutes=5)
        end = reference
        id_venue = "1aa901c4-2315-4b71-a871-a0a506f90ff1"  # Euregio
        venue = Venue.objects.filter(pk=id_venue).first()
        if venue is None:
            raise WebHookException(f"venue {id_venue} not found")
        new = PseudoRadarBulletin(
            id_venue=venue,
            reference=reference,
            start=start,
            end=end,
            last_update=last_update,
            author=key_prefix,
            version=0,
            draft=is_draft,
        )
        new.save()

        # trigger asynchronous post-processing, adding a job in the Celery task queue
        on_commit(lambda: tasks.post_process_radar.delay(new.id, f.name))

    content = {"status": "OK"}
    return Response(content)


@csrf_exempt
@api_view(["POST"])
@permission_classes([HasAPIKey])
def models_webhook(request):
    if request.content_type != "application/zip":
        return HttpResponseBadRequest("Please send data as application/zip")
    if int(request.META["CONTENT_LENGTH"]) == 0:
        return HttpResponseBadRequest("CONTENT_LENGTH is zero")
    if len(request.body) == 0:
        return HttpResponseBadRequest("body length is zero")

    with tempfile.NamedTemporaryFile(delete=False) as f:
        f.write(request.body)
        f.flush()
        try:
            with zipfile.ZipFile(f.name, "r") as zip_ref:
                test = zip_ref.testzip()
                if test is not None:
                    return HttpResponseBadRequest(f"ZIP: file CRC’s / headers corrupted: {test}")
        except Exception:
            return HttpResponseBadRequest("ZIP: Unknown file format")

        # trigger asynchronous post-processing, adding a job in the Celery task queue
        on_commit(lambda: tasks.post_process_models.delay(f.name))

    content = {"status": "OK"}
    return Response(content)


class ReadOnly(BasePermission):
    def has_permission(self, request, view):
        return request.method in SAFE_METHODS


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 100


class SkyConditionView(ModelViewSet):
    queryset = SkyCondition.objects.all().order_by("id")
    serializer_class = SkyConditionSerializer
    permission_classes = [ReadOnly]
    http_method_names = ["get", "head"]


VENUES_CHOICES = (
    (0, 0),
    (1, 1),
    (2, 2),
    (3, 3),
    (4, 4),
    (5, 5),
    (6, 6),
    (7, 7),
    (8, 8),
)


class VenuesFilter(filters.FilterSet):
    id_venue_type = filters.MultipleChoiceFilter(choices=VENUES_CHOICES)

    class Meta:
        model = Venue
        fields = ["id_venue_type", "id_region"]


def distance(lat1, lon1, lat2, lon2):
    # distance with the haversine formula: http://www.movable-type.co.uk/scripts/latlong.html
    # lat, lon in sexagesimal degrees
    # distance in km
    # sample calculations:
    #
    # distance(45.466944, 9.19, 45.066667, 7.7) = 124.84546433874874
    # distance(0, 0, 0, 0) = 0.0
    # distance(0, 0, 90, 0) = 10010.366119009535
    # distance(0, 0, 180, 0) = 20020.73223801907
    # distance(0, 0, 90, 0) = 10010.366119009535
    # distance(0, 0, 180, 0) = 20020.73223801907
    D = 12745.594
    R = D / 2.0
    phi1 = lat1 * math.pi / 180.0
    phi2 = lat2 * math.pi / 180.0
    delta_phi = (lat2 - lat1) * math.pi / 180.0
    delta_lambda = (lon2 - lon1) * math.pi / 180.0
    a = math.sin(delta_phi / 2.0) * math.sin(delta_phi / 2.0) + math.cos(phi1) * math.cos(phi2) * math.sin(
        delta_lambda / 2.0
    ) * math.sin(delta_lambda / 2.0)
    c = 2.0 * math.atan2(math.sqrt(a), math.sqrt(1.0 - a))
    d = R * c
    return d


def distances(venue, venues, j, k):
    # returns a dict of distances in km indexed by venues.id for the [j:k] venues closest to venue
    distances = {}
    for other in venues:
        distances[other["id"]] = distance(venue["lat"], venue["lon"], other["lat"], other["lon"])
    return dict(list({k: v for k, v in sorted(distances.items(), key=lambda item: item[1])}.items())[j:k])


class VenueTypeView(ModelViewSet):
    queryset = VenueType.objects.all()
    serializer_class = VenueTypeSerializer
    permission_classes = [ReadOnly]
    http_method_names = ["get", "head"]


class VenueView(ModelViewSet):
    queryset = Venue.objects.all().order_by("id")
    serializer_class = VenueSerializer
    permission_classes = [ReadOnly]
    filterset_class = VenuesFilter
    http_method_names = ["get", "head"]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        serializer = self.get_serializer(queryset, many=True)

        towns = [venue for venue in serializer.data if venue["id_venue_type"] == "2"]
        stations = [venue for venue in serializer.data if venue["id_venue_type"] == "3"]
        webcams = [venue for venue in serializer.data if venue["id_venue_type"] == "6"]
        for town in towns:
            near_stations = distances(town, stations, 0, 3)
            near_towns = distances(town, towns, 1, 4)
            near_webcam = distances(town, webcams, 0, 3)
            town["neighbors"] = {
                "stations": near_stations,
                "towns": near_towns,
                "webcams": near_webcam,
            }
        for station in stations:
            near_stations = distances(station, stations, 1, 4)
            near_towns = distances(station, towns, 0, 3)
            near_webcam = distances(station, webcams, 0, 3)
            station["neighbors"] = {
                "stations": near_stations,
                "towns": near_towns,
                "webcams": near_webcam,
            }
        for webcam in webcams:
            near_stations = distances(webcam, stations, 0, 3)
            near_towns = distances(webcam, towns, 0, 3)
            near_webcam = distances(webcam, webcams, 1, 4)
            webcam["neighbors"] = {
                "stations": near_stations,
                "towns": near_towns,
                "webcams": near_webcam,
            }
        return Response(serializer.data)


class WebcamView(ModelViewSet):
    queryset = Webcam.objects.all()
    permission_classes = [ReadOnly]
    serializer_class = WebcamSerializerDynamic


class DataView(ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer
    permission_classes = [ReadOnly]
    pagination_class = StandardResultsSetPagination
    http_method_names = ["get", "head"]


class DataTypeView(ModelViewSet):
    queryset = DataType.objects.all()
    serializer_class = DataTypeSerializer
    permission_classes = [ReadOnly]
    http_method_names = ["get", "head"]

    def retrieve(self, request, pk=None):
        queryset = DataType.objects
        dt = get_object_or_404(queryset, pk=pk)
        return Response(dt.schema)


class CurrentDataFilter(filters.FilterSet):
    start = filters.DateTimeFilter(field_name="start", lookup_expr="gte")
    end = filters.DateTimeFilter(field_name="end", lookup_expr="lte")

    class Meta:
        model = CurrentData
        fields = ["id_venue", "id_data_type"]


class CurrentDataView(ModelViewSet):
    queryset = CurrentData.objects.all()
    serializer_class = CurrentDataViewSerializer
    permission_classes = [ReadOnly]
    filterset_class = CurrentDataFilter
    http_method_names = ["get", "head"]


class PreviTempView(ModelViewSet):
    venues = [
        "e0c04e2d-8221-48d3-92b9-56e26e743213",
        "d84f35fa-b3dd-4172-b3ac-bf0989f17d43",
        "69793d0f-ca9a-4779-9cf8-9db1e2fe3459",
        "6064a14c-3483-44c4-a64c-45b471b4783c",
        "db8d25d3-f7e9-4cf7-8e98-dc57e6c40a11",
        "034a30f4-aa50-411f-948f-d4365f1be53f",
        "d1d9e526-6f50-40ef-93de-7b2f78316d7f",
        "b0d0e3fc-01fe-4004-adf0-d560669bc7f4",
        "b3d3402d-30f3-4ca8-b1ef-166b5999e2a3",
        "d2aa48d3-72db-4b1f-a694-6457b8d350e2",
        "106202e5-b797-47bf-8434-eb3f88a80791",
        "358309dc-78e9-4f6c-8681-c96079d9ea49",
        "02cd7dd1-aa55-40f6-a000-b9050e4695af",
        "23bf9fa6-3255-4a5a-a714-2ca30ca6a809",
        "c0808b1a-c732-41f4-94be-79823d15cbe6",
        "2d3dd4a1-d81c-44fa-8734-60c61ed23aff",
        "ceca8894-abbc-4ba3-b0ee-e8150b7e3a9f",
        "b33a3c75-6a80-4d69-bfd6-6a83bd863928",
        "523f9dd3-363f-4a3f-a720-5e833a391975",
        "1e083c5e-19b2-477e-84a1-d71f2356f1ec",
        "d4919a62-1a1b-4d25-8164-4bd37b915dc0",
        "68cf5878-23f5-4cc1-afa4-2ec595abc9b3",
        "0867c080-d2f7-4607-bd32-fb50b47fc91e",
        "cbc65c6e-82aa-4e27-80ab-6a2d56479133",
        "3c4bc6bf-212c-4a2a-8556-9689ce2ca9ab",
        "a35c1ca5-1a8a-4db2-89be-8fa4b5c0bee0",
        "186ca596-abfc-4882-ac14-999ed5edc6c6",
        "4c6eaca3-6537-4f5b-80bf-ff5f792b84c7",
        "fbb753cb-9afb-4090-a1f0-2e9531c0b60c",
        "485df9c2-83df-4915-8e7e-31c02951303a",
        "27b0481d-1e45-4fb8-8fe8-72c26e339e8a",
        "3ff421b6-233e-4f3a-970d-8456a66fa315",
        "0f1b5876-9325-4685-a471-1938cf39ec80",
        "a2d361d8-cf6e-483f-ac37-443b5e5b1c3c",
        "3a541c02-d2f4-4eb6-b300-88ae21a009be",
        "4aaa7281-668a-4976-92cf-d2c051944020",
    ]
    queryset = CurrentData.objects.filter(id_venue__in=venues)
    serializer_class = CurrentDataViewSerializer
    permission_classes = [ReadOnly]
    http_method_names = ["get", "head"]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        current_data = serializer.data
        temperatures = {}
        for record in current_data:
            if record["id_time_layout"] is not None and record["id_time_layout"] >= 144000000:
                if record["id_venue"] not in temperatures:
                    temperatures[record["id_venue"]] = {}
                temperatures[record["id_venue"]][record["id_time_layout"]] = {
                    "temperature_maximum": record["value"]["temperature_maximum"],
                    "temperature_minimum": record["value"]["temperature_minimum"],
                }
        return Response(temperatures)


class CurrentTextsFilter(filters.FilterSet):
    start = filters.DateTimeFilter(field_name="start", lookup_expr="gte")
    end = filters.DateTimeFilter(field_name="end", lookup_expr="lte")

    class Meta:
        model = CurrentTexts
        fields = ["id_venue"]


class CurrentTextsView(ModelViewSet):
    queryset = CurrentTexts.objects.all()
    serializer_class = CurrentTextsViewSerializer
    permission_classes = [ReadOnly]
    filterset_class = CurrentTextsFilter
    http_method_names = ["get", "head"]


class TimeLayoutFilter(filters.FilterSet):
    start = filters.TimeFilter(field_name="start_time", lookup_expr="gte")
    end = filters.TimeFilter(field_name="end_time", lookup_expr="lte")

    class Meta:
        model = TimeLayout
        fields = ["start_day_offset", "end_day_offset", "minutes"]


class TimeLayoutView(ModelViewSet):
    queryset = TimeLayout.objects.all()
    serializer_class = TimeLayoutSerializer
    permission_classes = [ReadOnly]
    filterset_class = TimeLayoutFilter
    http_method_names = ["get", "head"]


class ShortResultsSetPagination(PageNumberPagination):
    page_size = 10


class DataBulletinFilter(filters.FilterSet):
    class Meta:
        model = DataBulletin
        fields = ["id_venue"]


class DataBulletinView(ModelViewSet):
    queryset = DataBulletin.objects.order_by("-id")
    serializer_class = DataBulletinSerializer
    permission_classes = [ReadOnly]
    pagination_class = ShortResultsSetPagination
    filterset_class = DataBulletinFilter
    http_method_names = ["get", "head"]


class PseudoRadarBulletinView(ModelViewSet):
    queryset = PseudoRadarBulletin.objects.order_by("-id")
    serializer_class = PseudoRadarBulletinSerializer
    permission_classes = [ReadOnly]
    pagination_class = ShortResultsSetPagination
    http_method_names = ["get", "head"]


class PseudoRadarBulletinPageView(TemplateView):
    template_name = "pages/bulletin_pseudoradar.html"

    def get(self, request):
        if not request.user.is_authenticated:
            current_url = request.get_full_path()
            return HttpResponseRedirect(reverse("account_login") + "?next=" + current_url)
        else:
            return render(request, "pages/bulletin_pseudoradar.html")
