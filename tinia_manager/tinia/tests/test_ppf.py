import datetime
import json

import pytest
import pytz

from tinia_manager.tinia.models import Data, DataBulletin, DataType, TimeLayout, Venue, VenueType
from tinia_manager.tinia.tasks import post_process_forecast_bulletin


def forecast_data_bulletin(reference):
    data_type = DataType.objects.get(id="forecast_record_schema")
    town = VenueType.objects.get(description="Town")
    venue = Venue.objects.create(id_venue_type=town, name_eng="test venue", weight=1)
    start = reference.strftime("%Y-%m-%dT%H:%M:%S%z")
    end = (reference + datetime.timedelta(days=1) - datetime.timedelta(seconds=1)).strftime("%Y-%m-%dT%H:%M:%S%z")
    data_bulletin = DataBulletin.objects.create(
        id_venue=venue,
        reference=start,
        start=start,
        end=end,
        last_update=start,
        author="test author",
        version=1,
        draft=False,
        post_processed=False,
    )
    # print(f"DataBulletin: {data_bulletin.__dict__}")
    today = TimeLayout.objects.get(id=144000000)
    tomorrow = TimeLayout.objects.get(id=144001440)
    value = json.load(open("test/forecast_record.json"))
    Data.objects.create(
        id_data_bulletin=data_bulletin,
        id_venue=venue,
        id_time_layout=today,
        id_data_type=data_type,
        value=value,
    )
    Data.objects.create(
        id_data_bulletin=data_bulletin,
        id_venue=venue,
        id_time_layout=tomorrow,
        id_data_type=data_type,
        value=value,
    )
    for itl in range(18000000, 18000000 + 2 * 1440, 180):
        tl = TimeLayout.objects.get(id=itl)
        Data.objects.create(
            id_data_bulletin=data_bulletin,
            id_venue=venue,
            id_time_layout=tl,
            id_data_type=data_type,
            value=value,
        )
    return data_bulletin


@pytest.mark.django_db
def test_post_process_forecast_bulletin_cet():
    tz = pytz.timezone("CET")
    reference = datetime.datetime.now(tz).replace(hour=0, minute=0, second=0, microsecond=0)
    data_bulletin = forecast_data_bulletin(reference)
    id_data_bulletin = data_bulletin.id
    saved_files = post_process_forecast_bulletin(id_data_bulletin, "/tmp/test", skip_rsync=True)
    assert saved_files == 2  # mountains bulletin.json, forecasts bulletin.json
    bulletin = DataBulletin.objects.get(id=id_data_bulletin)
    assert bulletin.post_processed


@pytest.mark.django_db
def test_post_process_forecast_bulletin_utc():
    reference = datetime.datetime.combine(datetime.date.today(), datetime.time.min, datetime.timezone.utc)
    data_bulletin = forecast_data_bulletin(reference)
    id_data_bulletin = data_bulletin.id
    saved_files = post_process_forecast_bulletin(id_data_bulletin, "/tmp/test", skip_rsync=True)
    assert saved_files == 21  # mountains bulletin.json, forecasts bulletin.json, 1 venue, 2 days, 16 3-hourly intervals
    bulletin = DataBulletin.objects.get(id=id_data_bulletin)
    assert bulletin.post_processed
