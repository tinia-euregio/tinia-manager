import math
from datetime import date, datetime, time, timezone

from django.utils import timezone as tz
from rest_framework import serializers
from rest_framework.fields import Field

from tinia_manager.tinia import models

utc_tz = timezone.utc
tls_lookup = {}
current_tz = tz.get_current_timezone()
date_format_human = "%A, %d-%m-%Y, %H:%M, %Z"


def get_today_midnight():
    return datetime.combine(date.today(), time.min, timezone.utc)


def format_timestamp_human(value):
    return value.astimezone(current_tz).strftime(date_format_human)


def format_timestamp_human_utc(value):
    return value.astimezone(timezone.utc).strftime(date_format_human)


class TimeStampNaiveField(Field):
    def to_representation(self, value):
        if not value:
            return None

        if isinstance(value, str):
            return value

        return tz.make_naive(value, current_tz).strftime("%Y-%m-%dT%H:%M:%S")


class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Data
        fields = "__all__"


class DataTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.DataType
        fields = ["id", "description"]


class DataTypeSerializerDetail(serializers.ModelSerializer):
    class Meta:
        model = models.DataType
        fields = ["schema"]


class SkyConditionSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.SkyCondition
        fields = "__all__"


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Text
        fields = "__all__"


class WebcamSerializerStatic(serializers.ModelSerializer):
    class Meta:
        model = models.Webcam
        fields = ["link", "image_url"]


class WebcamSerializerDynamic(serializers.ModelSerializer):
    class Meta:
        model = models.Webcam
        fields = ["active", "alive", "venue_id"]


class VenueTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.VenueType
        fields = "__all__"


class VenueSerializer(serializers.ModelSerializer):
    webcam = WebcamSerializerStatic(many=False, read_only=True)

    class Meta:
        model = models.Venue
        fields = "__all__"


class CurrentDataViewSerializer(serializers.ModelSerializer):
    id_time_layout = serializers.SerializerMethodField()
    start = TimeStampNaiveField()
    end = TimeStampNaiveField()

    class Meta:
        model = models.CurrentData
        fields = "__all__"

    def get_id_time_layout(self, obj):
        global tls_lookup
        if tls_lookup == {}:
            start_day_offsets = [tl.start_day_offset for tl in models.TimeLayout.objects.distinct("start_day_offset")]
            minutes = [tl.minutes for tl in models.TimeLayout.objects.distinct("minutes")]
            tls_lookup = {sdo: {m: {} for m in minutes} for sdo in start_day_offsets}
            tls = models.TimeLayout.objects.all()
            for tl in tls:
                if tl.start_day_offset in tls_lookup:
                    tls_lookup[tl.start_day_offset][tl.minutes][tl.start_time] = tl.id
        midnight = get_today_midnight()
        delta = obj.start - midnight
        start_day_offset = math.floor(delta.total_seconds() / 86400)
        minutes = round((obj.end - obj.start).total_seconds() / 60)
        start_time = obj.start.astimezone(utc_tz).time()
        if start_day_offset in tls_lookup:
            lu = tls_lookup[start_day_offset][minutes]
            if start_time in lu:
                return lu[start_time]
            else:
                # print(f"start time {start_time} not found")
                return None
        else:
            # print(f"start day offset {start_day_offset} not found")
            return None


class TimeLayoutSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.TimeLayout
        fields = "__all__"


class CurrentTextsViewSerializer(serializers.ModelSerializer):
    id_time_layout = serializers.SerializerMethodField()
    start = TimeStampNaiveField()
    end = TimeStampNaiveField()

    class Meta:
        model = models.CurrentTexts
        fields = "__all__"

    def get_id_time_layout(self, obj):
        global tls_lookup
        if tls_lookup == {}:
            start_day_offsets = [tl.start_day_offset for tl in models.TimeLayout.objects.distinct("start_day_offset")]
            minutes = [tl.minutes for tl in models.TimeLayout.objects.distinct("minutes")]
            tls_lookup = {sdo: {m: {} for m in minutes} for sdo in start_day_offsets}
            tls = models.TimeLayout.objects.all()
            for tl in tls:
                if tl.start_day_offset in tls_lookup:
                    tls_lookup[tl.start_day_offset][tl.minutes][tl.start_time] = tl.id
        midnight = get_today_midnight()
        delta = obj.start - midnight
        start_day_offset = math.floor(delta.total_seconds() / 86400)
        minutes = round((obj.end - obj.start).total_seconds() / 60)
        start_time = obj.start.astimezone(utc_tz).time()
        if start_day_offset in tls_lookup:
            lu = tls_lookup[start_day_offset][minutes]
            if start_time in lu:
                return lu[start_time]
            else:
                # print(f"start time {start_time} not found")
                return None
        else:
            # print(f"start day offset {start_day_offset} not found")
            return None


class DataBulletinSerializer(serializers.ModelSerializer):
    data_num = serializers.ReadOnlyField(source="datas.count")

    class Meta:
        model = models.DataBulletin
        fields = "__all__"


class TextBulletinSerializer(serializers.ModelSerializer):
    texts = TextSerializer(many=True, read_only=True)

    class Meta:
        model = models.TextBulletin
        fields = "__all__"


class PseudoRadarBulletinSerializer(serializers.ModelSerializer):
    start_human = serializers.SerializerMethodField()
    end_human = serializers.SerializerMethodField()
    last_update_human = serializers.SerializerMethodField()
    start_human_utc = serializers.SerializerMethodField()
    end_human_utc = serializers.SerializerMethodField()
    last_update_human_utc = serializers.SerializerMethodField()

    def get_start_human(self, obj):
        return format_timestamp_human(obj.start)

    def get_end_human(self, obj):
        return format_timestamp_human(obj.end)

    def get_last_update_human(self, obj):
        return format_timestamp_human(obj.last_update)

    def get_start_human_utc(self, obj):
        return format_timestamp_human_utc(obj.start)

    def get_end_human_utc(self, obj):
        return format_timestamp_human_utc(obj.end)

    def get_last_update_human_utc(self, obj):
        return format_timestamp_human_utc(obj.last_update)

    class Meta:
        model = models.PseudoRadarBulletin
        fields = "__all__"
