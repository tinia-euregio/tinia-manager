import uuid

from django.db import models
from django.db.models import JSONField


class TimeLayout(models.Model):
    id = models.IntegerField(primary_key=True)
    start_day_offset = models.SmallIntegerField()
    end_day_offset = models.SmallIntegerField(blank=True, null=True)
    start_time = models.TimeField()
    end_time = models.TimeField()
    minutes = models.IntegerField()
    one_day_minutes = 1440

    class Meta:
        db_table = "time_layouts"

    @staticmethod
    def get_mountain_day():
        return TimeLayout.objects.get(start_day_offset=0, minutes=4320)

    def get_start_day():
        return TimeLayout.objects.get(start_day_offset=0, minutes=TimeLayout.one_day_minutes)

    def get_second_day():
        return TimeLayout.objects.get(start_day_offset=1, minutes=TimeLayout.one_day_minutes)

    def get_third_day():
        return TimeLayout.objects.get(start_day_offset=2, minutes=TimeLayout.one_day_minutes)

    def get_fourth_day():
        return TimeLayout.objects.get(start_day_offset=3, minutes=TimeLayout.one_day_minutes)

    def get_fifth_day():
        return TimeLayout.objects.get(start_day_offset=4, minutes=TimeLayout.one_day_minutes)

    def get_last_day():
        return TimeLayout.objects.get(start_day_offset=5, minutes=TimeLayout.one_day_minutes)


class VenueType(models.Model):
    id = models.TextField(primary_key=True)
    description = models.TextField(blank=True, null=False)

    class Meta:
        db_table = "venue_types"


class Venue(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    elevation = models.IntegerField(null=True)
    lat = models.FloatField(null=True)
    lon = models.FloatField(null=True)
    name_deu = models.TextField(blank=True, null=False)
    name_eng = models.TextField(blank=True, null=False)
    name_ita = models.TextField(blank=True, null=False)
    name_lld = models.TextField(blank=True, null=False)
    weight = models.IntegerField()
    id_venue_type = models.ForeignKey(VenueType, models.DO_NOTHING, db_column="id_venue_type")
    id_region = models.ForeignKey("self", on_delete=models.CASCADE, db_column="id_region", null=True)

    class Meta:
        db_table = "venues"

    def __str__(self):
        return self.name_eng


class Webcam(models.Model):
    venue = models.OneToOneField(
        Venue,
        on_delete=models.CASCADE,
        primary_key=True,
    )
    image_url = models.TextField(
        null=False, db_comment="URL to the image, can be a template using strftime/strptime format codes"
    )
    link = models.TextField(null=False, db_comment="URL to the webcam page for attribution")
    active = models.BooleanField(default=True, db_comment="Last time we checked, the webcam image_url responded")
    alive = models.BooleanField(
        default=False,
        db_comment="Last time we checked, the webcam image_url responded with an image different from the last MAX_REPEATED_IMG",
    )
    checksums = models.JSONField(
        null=True,
        db_comment="The SHA256 sums of the last MAX_REPEATED_IMG retrieved images stored as an array of strings, the first being the most recent",
    )
    last_checked = models.DateTimeField(null=True, db_comment="The last time we checked the webcam image_url")

    class Meta:
        db_table = "webcams"


class DataType(models.Model):
    id = models.TextField(primary_key=True)
    description = models.TextField(blank=True, null=False)
    schema = models.TextField(blank=True, null=False)
    schema = JSONField()

    class Meta:
        db_table = "data_types"


class DataBulletin(models.Model):
    id = models.AutoField(primary_key=True)
    id_venue = models.ForeignKey(Venue, models.DO_NOTHING, db_column="id_venue")
    reference = models.DateTimeField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    last_update = models.DateTimeField()
    author = models.TextField(blank=True, null=False)
    version = models.IntegerField(blank=True, null=True)
    draft = models.BooleanField(default=True)
    post_processed = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = "data_bulletins"


class Data(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_data_bulletin = models.ForeignKey(
        DataBulletin,
        models.DO_NOTHING,
        db_column="id_data_bulletin",
        related_name="datas",
    )
    id_venue = models.ForeignKey(Venue, models.DO_NOTHING, db_column="id_venue")
    id_time_layout = models.ForeignKey(TimeLayout, models.DO_NOTHING, db_column="id_time_layout")
    id_data_type = models.ForeignKey(DataType, models.DO_NOTHING, db_column="id_data_type")
    value = JSONField()

    class Meta:
        db_table = "data"
        indexes = [
            models.Index(
                fields=[
                    "id_data_bulletin",
                ]
            ),
        ]


class CurrentData(models.Model):
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    id_venue = models.UUIDField(blank=True, null=True)
    id_data_type = models.ForeignKey(DataType, models.DO_NOTHING, db_column="id_data_type")
    value = models.JSONField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "currentdata"


class TextBulletin(models.Model):
    id = models.AutoField(primary_key=True)
    id_venue = models.ForeignKey(Venue, models.DO_NOTHING, db_column="id_venue")
    reference = models.DateTimeField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    last_update = models.DateTimeField()
    author = models.TextField(blank=True, null=False)
    version = models.IntegerField(blank=True, null=True)
    draft = models.BooleanField(default=True)
    post_processed = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = "text_bulletins"


class Text(models.Model):
    id = models.BigAutoField(primary_key=True)
    id_text_bulletin = models.ForeignKey(
        TextBulletin,
        models.DO_NOTHING,
        db_column="id_text_bulletin",
        related_name="texts",
    )
    id_venue = models.ForeignKey(Venue, models.DO_NOTHING, db_column="id_venue")
    value_deu = models.TextField(blank=True, null=False)
    value_eng = models.TextField(blank=True, null=False)
    value_ita = models.TextField(blank=True, null=False)
    value_lld = models.TextField(blank=True, null=False)
    id_time_layout = models.ForeignKey(TimeLayout, models.DO_NOTHING, db_column="id_time_layout")

    class Meta:
        db_table = "texts"

    @staticmethod
    def create_new_text(newbulletin, region_id, time_layout):
        return Text.objects.create(
            id_text_bulletin=newbulletin,
            id_venue_id=region_id,
            id_time_layout=time_layout,
            value_eng="",
            value_deu="",
            value_ita="",
            value_lld="",
        )


class SkyCondition(models.Model):
    id = models.CharField(max_length=1, primary_key=True)
    icon_day = models.TextField(blank=True, null=False)
    icon_night = models.TextField(blank=True, null=False)
    name_deu = models.TextField(blank=True, null=False)
    name_eng = models.TextField(blank=True, null=False)
    name_ita = models.TextField(blank=True, null=False)
    name_lld = models.TextField(blank=True, null=False)
    svg_day = models.TextField(blank=True, null=False)
    svg_night = models.TextField(blank=True, null=False)

    class Meta:
        db_table = "sky_conditions"


class CurrentTexts(models.Model):
    start = models.DateTimeField(blank=True, null=True)
    end = models.DateTimeField(blank=True, null=True)
    author = models.TextField(blank=True, null=False)
    id_venue = models.UUIDField(blank=True, null=True)
    value_deu = models.TextField(blank=True, null=False)
    value_eng = models.TextField(blank=True, null=False)
    value_ita = models.TextField(blank=True, null=False)
    value_lld = models.TextField(blank=True, null=False)
    last_update = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False  # Created from a view. Don't remove.
        db_table = "currenttexts"


class PseudoRadarBulletin(models.Model):
    id = models.AutoField(primary_key=True)
    id_venue = models.ForeignKey(Venue, models.DO_NOTHING, db_column="id_venue")
    reference = models.DateTimeField()
    start = models.DateTimeField()
    end = models.DateTimeField()
    last_update = models.DateTimeField()
    author = models.TextField(blank=True, null=False)
    version = models.IntegerField(blank=True, null=True)
    draft = models.BooleanField(default=True)
    post_processed = models.BooleanField(blank=True, null=True)

    class Meta:
        db_table = "pseudoradar_bulletins"
