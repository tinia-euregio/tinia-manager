import asyncio
import datetime
import hashlib
import io
import json
import logging
import math
import os
import pathlib
import tempfile
import time
import zipfile
from subprocess import call
from uuid import UUID

import aiohttp
import netCDF4
import requests
from django.db import transaction
from django.utils import timezone as tz
from PIL import Image
from pyproj import CRS, Transformer

from config import celery_app
from config.settings.base import env
from tinia_manager.tinia.models import (
    CurrentData,
    CurrentTexts,
    DataBulletin,
    PseudoRadarBulletin,
    TextBulletin,
    Venue,
    VenueType,
    Webcam,
)
from tinia_manager.tinia.serializers import (
    CurrentDataViewSerializer,
    CurrentTextsViewSerializer,
    TextBulletinSerializer,
    WebcamSerializerDynamic,
)

date_format = "%Y-%m-%dT%H:%M:%S"
current_tz = tz.get_current_timezone()
mutable_dir = "var"
logger = logging.getLogger(__name__)

# number of webcam image checksums to keep
MAX_REPEATED_IMG = 3


def format_timestamp(value):
    return tz.make_naive(value, current_tz).strftime(date_format)


# https://stackoverflow.com/questions/36588126/uuid-is-not-json-serializable
class UUIDEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, UUID):
            return str(obj)
        return json.JSONEncoder.default(self, obj)


class TaskFailure(Exception):
    def __init__(self, message):
        super().__init__(message)
        logger.error(f"Task failiure: {message}")


def get_url(url_template, hour_offset):
    # subtract hour_offset from now
    # the url_template can use strftime/strptime format codes, see: https://strftime.org
    zp = datetime.datetime.now() - datetime.timedelta(hours=hour_offset)
    return zp.strftime(url_template)


def min_stamp(s1, s2):
    ts1 = datetime.datetime.strptime(s1, date_format)
    ts2 = datetime.datetime.strptime(s2, date_format)
    if ts1 > ts2:
        return s2
    else:
        return s1


def max_stamp(s1, s2):
    ts1 = datetime.datetime.strptime(s1, date_format)
    ts2 = datetime.datetime.strptime(s2, date_format)
    if ts1 > ts2:
        return s1
    else:
        return s2


def chunks(arr, n):
    # https://stackoverflow.com/a/54802737
    """Yield n number of striped chunks from arr."""
    for i in range(0, n):
        yield arr[i::n]


def zero_pad(n):
    return f"{n:03}"


def general_post_process_forecasts(skip_rsync, forecasts, folder):
    saved_files = 0
    td = get_today_midnight()
    with tempfile.TemporaryDirectory() as dir:
        tls180 = {}
        tls1440 = {}
        venues = {}

        for forecast in forecasts:
            itl = forecast["id_time_layout"]
            if itl and round(itl / 1e5) == 180:
                # 3-hours forecasts
                itl180 = itl
                venue = forecast["id_venue"]
                if itl180 is not None:
                    if itl180 in tls180:
                        tls180[itl180][venue] = forecast["value"]
                    else:
                        tls180[itl180] = {
                            "start": forecast["start"],
                            "end": forecast["end"],
                            venue: forecast["value"],
                        }
                    if venue in venues:
                        venues[venue]["180"][itl180] = forecast["value"]
                        venues[venue]["start"] = min_stamp(venues[venue]["start"], forecast["start"])
                        venues[venue]["end"] = max_stamp(venues[venue]["end"], forecast["end"])
                    else:
                        venues[venue] = {
                            "start": forecast["start"],
                            "end": forecast["end"],
                            "180": {
                                itl180: forecast["value"],
                            },
                            "1440": {},
                        }
        for forecast in forecasts:
            itl = forecast["id_time_layout"]
            if itl and round(itl / 1e5) == 1440:
                # 24-hours aggregated forecasts
                itl1440 = itl
                venue = forecast["id_venue"]
                if itl1440 is not None:
                    if itl1440 in tls1440:
                        tls1440[itl1440][venue] = forecast["value"]
                    else:
                        tls1440[itl1440] = {
                            "start": forecast["start"],
                            "end": forecast["end"],
                            venue: forecast["value"],
                        }
                    if venue in venues:
                        venues[venue]["1440"][itl1440] = forecast["value"]
                        venues[venue]["start"] = min_stamp(venues[venue]["start"], forecast["start"])
                        venues[venue]["end"] = max_stamp(venues[venue]["end"], forecast["end"])
                    else:
                        venues[venue] = {
                            "start": forecast["start"],
                            "end": forecast["end"],
                            "1440": {
                                itl1440: forecast["value"],
                            },
                            "180": {},
                        }

        for forecast in forecasts:
            itl = forecast["id_time_layout"]
            if itl and not round(itl / 1e5) == 180 and not round(itl / 1e5) == 1440:
                error = f"unexpected time layout {itl}"
                raise TaskFailure(error)

        for tl in tls180:
            with open(f"{dir}/{tl}.json", "w") as tl_file:
                json.dump(tls180[tl], tl_file, allow_nan=False)
                tl_file.flush()
                saved_files += 1

        for tl in tls1440:
            with open(f"{dir}/{tl}.json", "w") as tl_file:
                json.dump(tls1440[tl], tl_file, allow_nan=False)
                tl_file.flush()
                saved_files += 1

        for venue in venues:
            days = len(venues[venue]["1440"])
            for day in range(0, days):
                itl180_first = 18000000 + day * 1440
                itl1440 = 144000000 + day * 1440
                rain_fall = 0
                for segment_3h in range(0, 8):
                    itl180 = itl180_first + segment_3h * 180
                    if itl180 in venues[venue]["180"]:
                        value_rf = venues[venue]["180"][itl180]["rain_fall"]
                        if value_rf is not None:
                            rain_fall += value_rf
                venues[venue]["1440"][itl1440].update(
                    {
                        "rain_fall": round(rain_fall, 1),
                    }
                )
            with open(f"{dir}/{venue}.json", "w") as venue_file:
                json.dump(venues[venue], venue_file, allow_nan=False)
                venue_file.flush()
                saved_files += 1

        index = {
            "reference": format_timestamp(td),
            "1440": [],
            "instant": [],
            "current_1440": 0,
            "current_instant": 0,
        }

        tl1440 = 144000000
        tl180 = 18000000

        for i in range(7):
            tmp = {"id": tl1440, "start": format_timestamp(td)}
            index["1440"].append(tmp)
            tl1440 = tl1440 + 1440
            td = td + datetime.timedelta(days=1)

        td = get_today_midnight()

        for i in range(48):
            tmp = {"id": tl180, "start": format_timestamp(td)}
            index["instant"].append(tmp)
            tl180 = tl180 + 180
            td = td + datetime.timedelta(hours=3)

        with open(f"{dir}/bulletin.json", "w") as bulletin_file:
            json.dump(index, bulletin_file, allow_nan=False)
            bulletin_file.flush()
            saved_files += 1

        # call(["ls", "-lR", f"{dir}/"])

        if not skip_rsync:
            command = [
                "/usr/bin/rsync",
                "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
                "-avz",
                "--mkpath",
                "--delete",
                f"{dir}/",
                f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/data/{folder}",
            ]
            retcode = call(command)
            if retcode != 0:
                error = "rsync failed with code: %d" % retcode
                raise TaskFailure(error)

        return saved_files


def get_today_midnight():
    return datetime.datetime.combine(datetime.date.today(), datetime.time.min, datetime.timezone.utc)


@celery_app.task()
def post_process_forecasts(fname="", skip_rsync=False):
    saved_files = 0
    td = get_today_midnight()
    queryset = CurrentData.objects.all().filter(id_data_type="forecast_record_schema", start__gte=td)
    serializer = CurrentDataViewSerializer(queryset, many=True)
    forecasts = json.loads(json.dumps(serializer.data, cls=UUIDEncoder))

    # split venues
    venues_id_venue_type_2 = []
    venues_id_venue_type_5 = []
    for venue in Venue.objects.all():
        venue_type = int(venue.id_venue_type.id)
        if venue_type == 2:
            venues_id_venue_type_2.append(str(venue.id))
        elif venue_type == 5:
            venues_id_venue_type_5.append(str(venue.id))

    # split forecasts
    forecasts_id_venue_type_2 = []
    forecasts_id_venue_type_5 = []
    for forecast in forecasts:
        forecast["value"]["wind_speed"] = forecast["value"]["wind_speed"] * 3.6
        forecast["value"]["wind_gust"] = forecast["value"]["wind_gust"] * 3.6
        if forecast["id_venue"] in venues_id_venue_type_2:
            forecasts_id_venue_type_2.append(forecast)
        if forecast["id_venue"] in venues_id_venue_type_5:
            forecasts_id_venue_type_5.append(forecast)

    # rsync in data/forecasts/
    saved_files += general_post_process_forecasts(skip_rsync, forecasts_id_venue_type_2, "forecasts")

    # rsync in data/mountains/
    saved_files += general_post_process_forecasts(skip_rsync, forecasts_id_venue_type_5, "mountains")

    if "MIRROR_MANAGER" in os.environ and fname:
        mirror.delay("forecasts", fname)

    return saved_files


@celery_app.task()
def post_process_forecast_bulletin(id_data_bulletin, fname="", skip_rsync=False):
    data_bulletin = DataBulletin.objects.get(id=id_data_bulletin)
    data_bulletin.post_processed = False
    data_bulletin.save()

    with transaction.atomic():
        saved_files = post_process_forecasts(fname, skip_rsync)

        data_bulletin.post_processed = True
        data_bulletin.save()
    return saved_files


@celery_app.task()
def post_process_observations(id_data_bulletin, fname=""):
    saved_files = 0
    data_bulletin = DataBulletin.objects.get(id=id_data_bulletin)
    data_bulletin.post_processed = False
    data_bulletin.save()

    with transaction.atomic():
        td = get_today_midnight()
        tdm6 = td - datetime.timedelta(days=6)

        queryset = CurrentData.objects.all().filter(id_data_type="observation_record_schema", start__gte=tdm6)
        serializer = CurrentDataViewSerializer(queryset, many=True)
        observations = json.loads(json.dumps(serializer.data, cls=UUIDEncoder))

        with tempfile.TemporaryDirectory() as dir:
            tls30 = {}
            tls1440 = {}
            venues = {}
            max_itl30 = 2991360

            for observation in observations:
                if observation["value"]["rain_fall"] is not None:
                    observation["value"]["rain_fall"] = round(observation["value"]["rain_fall"], 1)
                if observation["value"]["wind_speed"] is not None:
                    observation["value"]["wind_speed"] = round((observation["value"]["wind_speed"] * 3.6), 1)
                if observation["value"]["wind_gust"] is not None:
                    observation["value"]["wind_gust"] = round((observation["value"]["wind_gust"] * 3.6), 1)
                if observation["value"]["pressure"] is not None:
                    observation["value"]["pressure"] = round(observation["value"]["pressure"])
                itl30 = observation["id_time_layout"]
                venue = observation["id_venue"]
                if itl30 is not None:
                    if itl30 > max_itl30:
                        max_itl30 = itl30
                    if itl30 in tls30:
                        tls30[itl30][venue] = observation["value"]
                    else:
                        tls30[itl30] = {
                            "start": observation["start"],
                            "end": observation["end"],
                            venue: observation["value"],
                        }
                    if venue in venues:
                        venues[venue]["30"][itl30] = observation["value"]
                        venues[venue]["start"] = min_stamp(venues[venue]["start"], observation["start"])
                        venues[venue]["end"] = max_stamp(venues[venue]["end"], observation["end"])
                    else:
                        venues[venue] = {
                            "start": observation["start"],
                            "end": observation["end"],
                            "30": {
                                itl30: observation["value"],
                            },
                        }
            for tl in tls30:
                with open(f"{dir}/{tl}.json", "w") as tl_file:
                    json.dump(tls30[tl], tl_file, allow_nan=False)
                    tl_file.flush()
                    saved_files += 1

            for venue in venues:
                # print("looking at venue:", venue)
                venues[venue]["1440"] = {}
                for day in range(-6, 1):
                    itl30_first = 3000000 + day * 1440
                    itl1440 = str(144000000 + day * 1440)
                    averages = {
                        "pressure": 0.0,
                        "wind_speed": 0.0,
                        "wind_direction": 0.0,
                        "relative_humidity": 0.0,
                    }
                    counts = {
                        "pressure": 0,
                        "wind_speed": 0,
                        "wind_direction": 0,
                        "relative_humidity": 0,
                    }
                    rain_fall = 0
                    temperature_minimum = 1000
                    temperature_maximum = -1000
                    wind_gust = 0
                    count = 0
                    if itl1440 in tls1440:
                        se = None
                    else:
                        se = {}
                    for segment_30min in range(0, 48):
                        itl30 = itl30_first + segment_30min * 30
                        if itl30 in venues[venue]["30"]:
                            if se is not None:
                                if "start" in se:
                                    se["start"] = min_stamp(tls30[itl30]["start"], se["start"])
                                    se["end"] = max_stamp(tls30[itl30]["end"], se["end"])
                                else:
                                    se = {
                                        "start": tls30[itl30]["start"],
                                        "end": tls30[itl30]["end"],
                                    }
                            for average in averages:
                                # print(f"venues[{venue}][30][{itl30}] = {venues[venue]['30'][itl30]}")
                                if average in venues[venue]["30"][itl30]:
                                    value = venues[venue]["30"][itl30][average]
                                    if value is not None:
                                        averages[average] += value
                                        counts[average] += 1
                            # print(venues[venue]["30"][itl30])
                            if "rain_fall" in venues[venue]["30"][itl30]:
                                value_rf = venues[venue]["30"][itl30]["rain_fall"]
                                if value_rf is not None:
                                    rain_fall += value_rf
                            if "temperature" in venues[venue]["30"][itl30]:
                                value_t = venues[venue]["30"][itl30]["temperature"]
                                if value_t is not None:
                                    if value_t < temperature_minimum:
                                        temperature_minimum = value_t
                                    if value_t > temperature_maximum:
                                        temperature_maximum = value_t
                            if "wind_gust" in venues[venue]["30"][itl30]:
                                value_wg = venues[venue]["30"][itl30]["wind_gust"]
                                if value_wg is not None:
                                    if value_wg > wind_gust:
                                        wind_gust = value_wg
                            count += 1
                    if count > 0:
                        if se and itl1440 not in tls1440:
                            tls1440[itl1440] = se
                        for average in averages:
                            if counts[average] > 0:
                                averages[average] /= counts[average]
                                if average == "pressure":
                                    averages[average] = round(averages[average])
                                else:
                                    averages[average] = round(averages[average], 1)
                            else:
                                averages[average] = None
                        venues[venue]["1440"][itl1440] = {}
                        venues[venue]["1440"][itl1440].update(averages)
                        if temperature_minimum > 999:
                            temperature_minimum = None
                        if temperature_maximum < -999:
                            temperature_maximum = None

                        aggregates = {
                            "rain_fall": rain_fall,
                            "temperature_minimum": temperature_minimum,
                            "temperature_maximum": temperature_maximum,
                            "wind_gust": wind_gust,
                        }

                        for aggregate in aggregates:
                            if aggregates[aggregate] is not None:
                                aggregates[aggregate] = round(aggregates[aggregate], 1)

                        venues[venue]["1440"][itl1440].update(aggregates)

                with open(f"{dir}/{venue}.json", "w") as venue_file:
                    json.dump(venues[venue], venue_file, allow_nan=False)
                    venue_file.flush()
                    saved_files += 1

            for venue in venues:
                # print(f"venues[{venue}][1440] = {venues[venue]['1440']}")
                for day in range(-6, 1):
                    itl1440 = str(day * 1440 + 144000000)
                    if itl1440 in venues[venue]["1440"]:
                        tls1440[itl1440][venue] = venues[venue]["1440"][itl1440]
            for tl in tls1440:
                # print("itl1440 =", tl)
                with open(f"{dir}/{tl}.json", "w") as tl_file:
                    json.dump(tls1440[tl], tl_file, allow_nan=False)
                    tl_file.flush()
                    saved_files += 1

            index = {
                "reference": format_timestamp(td),
                "1440": [],
                "instant": [],
                "current_1440": 6,
                "current_instant": math.floor((max_itl30 - 2991360) / 30),
            }

            tl1440 = 143991360
            tl30 = 2991360

            for i in range(8):
                tmp = {
                    "id": tl1440,
                    "start": format_timestamp(tdm6),
                    "end": format_timestamp(tdm6 + datetime.timedelta(days=1) - datetime.timedelta(seconds=1)),
                }
                index["1440"].append(tmp)
                tl1440 = tl1440 + 1440
                tdm6 = tdm6 + datetime.timedelta(days=1)

            tdm6 = td - datetime.timedelta(days=6)

            for i in range(336):
                tmp = {
                    "id": tl30,
                    "start": format_timestamp(tdm6),
                    "end": format_timestamp(tdm6 + datetime.timedelta(minutes=30) - datetime.timedelta(seconds=1)),
                }
                index["instant"].append(tmp)
                tl30 = tl30 + 30
                tdm6 = tdm6 + datetime.timedelta(minutes=30)

            with open(f"{dir}/bulletin.json", "w") as bulletin_file:
                json.dump(index, bulletin_file, allow_nan=False)
                bulletin_file.flush()
                saved_files += 1

            command = [
                "/usr/bin/rsync",
                "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
                "-avz",
                "--mkpath",
                "--delete",
                f"{dir}/",
                f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/data/observations",
            ]
            retcode = call(command)
            if retcode != 0:
                error = "rsync failed with code: %d" % retcode
                raise TaskFailure(error)

        data_bulletin.post_processed = True
        data_bulletin.save()

        if "MIRROR_MANAGER" in os.environ and fname:
            mirror.delay("observations", fname)

    return saved_files


@celery_app.task()
def post_process_texts():
    serializer = CurrentTextsViewSerializer(CurrentTexts.objects.all(), many=True)
    bulletins = json.loads(json.dumps(serializer.data, cls=UUIDEncoder))

    with tempfile.TemporaryDirectory() as dir:
        for alpha3 in ["eng", "deu", "ita"]:
            alpha2 = alpha3[:2]
            os.makedirs(f"{dir}/{alpha2}", exist_ok=False)
            value_alpha3 = "value_" + alpha3
            tmp = []
            for bulletin in bulletins:
                bulletin_copy = {k: v for k, v in bulletin.items() if "value_" not in k}
                bulletin_copy["value"] = bulletin[value_alpha3]
                tmp.append(bulletin_copy)
            with open(f"{dir}/{alpha2}/bulletins.json", "w") as file:
                file.write(json.dumps(tmp, indent=2))
        command = [
            "/usr/bin/rsync",
            "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
            "--mkpath",
            "-avz",
            f"{dir}/",
            f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/data/bulletins",
        ]
        retcode = call(command)
        if retcode != 0:
            error = "rsync failed with code: %d" % retcode
            raise TaskFailure(error)


@celery_app.task()
def post_process_models(fname):
    with zipfile.ZipFile(fname, "r") as zip_ref:
        with tempfile.TemporaryDirectory() as tmp_dir:
            zip_ref.extractall(tmp_dir)
            bulletin = f"{tmp_dir}/bulletin.json"
            command = [
                "/usr/bin/rsync",
                "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
                "-avz",
                "--mkpath",
                "--delete",
                bulletin,
                f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/data/models/bulletin.json",
            ]
            retcode = call(command)
            if retcode != 0:
                error = "rsync failed with code: %d" % retcode
                raise TaskFailure(error)
            os.remove(bulletin)
            command = [
                "/usr/bin/rsync",
                "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
                "-avz",
                "--mkpath",
                "--delete",
                f"{tmp_dir}/",
                f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/images/models",
            ]
            retcode = call(command)
            if retcode != 0:
                error = "rsync failed with code: %d" % retcode
                raise TaskFailure(error)

    if "MIRROR_MANAGER" in os.environ:
        mirror_models.delay(fname)


@celery_app.task()
def mirror_models(fname):
    url = os.environ["MIRROR_MANAGER"] + "/api/webhooks/models/"
    api_key = os.environ["MIRROR_MANAGER_APIKEY"]
    with open(fname, "rb") as f:
        data = f.read()
    response = requests.post(
        url, data=data, headers={"Authorization": f"Api-Key {api_key}", "Content-Type": "application/zip"}
    )
    os.unlink(fname)
    if response.status_code != 200:
        raise TaskFailure(f"mirror_models failed with code: {response.status_code} and content: {response.content}")


@celery_app.task()
def post_process_text_bulletin(id_text_bulletin):
    text_bulletin = TextBulletin.objects.get(id=id_text_bulletin)
    text_bulletin.post_processed = False
    text_bulletin.save()

    with transaction.atomic():
        post_process_texts()

        text_bulletin.post_processed = True
        text_bulletin.save()

    fname = f"/tmp/text_bulletin_{text_bulletin.pk}.json"
    serializer = TextBulletinSerializer(text_bulletin)
    text_bulletin = serializer.data
    json_bulletin = {
        "draft": text_bulletin["draft"],
        "start": text_bulletin["start"],
        "id_venue": text_bulletin["id_venue"],
        "id_data_type": "texts_message_schema",
        "dict": {},
        "data": [],
    }
    for text in text_bulletin["texts"]:
        id_venue = str(text["id_venue"])
        text.pop("id")
        text.pop("id_text_bulletin")
        text.pop("id_venue")
        text.pop("value_lld")
        if id_venue in json_bulletin["dict"]:
            json_bulletin["dict"][id_venue].append(text)
        else:
            json_bulletin["dict"][id_venue] = [text]
    for id_venue in json_bulletin["dict"]:
        json_bulletin["data"].append(
            {
                "id_venue": id_venue,
                "values": json_bulletin["dict"][id_venue],
            }
        )
    json_bulletin.pop("dict")
    with open(fname, "w") as f:
        f.write(json.dumps(json_bulletin, cls=UUIDEncoder))
    if "MIRROR_MANAGER" in os.environ:
        mirror.delay("texts", fname)

    return 1


@celery_app.task()
def mirror(endpoint, fname):
    url = os.environ["MIRROR_MANAGER"] + "/api/webhooks/" + endpoint + "/"
    api_key = os.environ["MIRROR_MANAGER_APIKEY"]
    with open(fname, "rt") as f:
        data = f.read()
    response = requests.post(
        url, data=data, headers={"Authorization": f"Api-Key {api_key}", "Content-Type": "application/json"}
    )
    if response.status_code != 200:
        raise TaskFailure(f"failed with code: {response.status_code} and content: {response.content}")


async def get_webcam_img(client, webcam, images_base):
    # print("  start webcam ===", webcam)
    image_url = webcam.image_url
    # print(f"looking at {webcam.venue_id}, image_url = {image_url}")
    if "%" in image_url:
        # look back up to 12 hours for a valid image
        hour_offsets = range(12)
        url_template = image_url
        for hour_offset in hour_offsets:
            image_url = get_url(url_template=url_template, hour_offset=hour_offset)
            # print(f"trying url = {image_url}")
            try:
                # r = requests.get(image_url)
                async with client.get(image_url) as response:
                    if response.status == 200:
                        # print(f"status === {r.status_code}, il link è {image_url}")
                        break
            except Exception as e:
                print(f"error get image_url: {e}")
                pass
    success = False
    try:
        async with client.get(image_url) as response:
            if response.status == 200:
                success = True
                img_data = await response.read()
    except Exception as e:
        print(f"error get image_url: {e}")
        pass
    if success:
        # webcam is active
        active = True
        # compute the sha256sum of the image
        new_checksum = hashlib.sha256(img_data).hexdigest()
        last_checked = datetime.datetime.now()
    else:
        # webcam is not active
        active = False
        # print(f"status code = {r.status_code}")
        # no luck: using placeholder image
        img_data = open("./tinia_manager/static/images/placeholder_image.jpg", "rb").read()
        new_checksum = hashlib.sha256(img_data).hexdigest()
        last_checked = datetime.datetime.now()

    # create thumbnail with PIL, but catch errors
    try:
        img_io = io.BytesIO(img_data)
        img = Image.open(img_io)
        width, height = img.size
        max_size = 256, 192
        # print(f"resizing image from {width}x{height} to 256x192")
        img.thumbnail(max_size, Image.BICUBIC)
        img_io = io.BytesIO()
        img.save(
            img_io,
            format="JPEG",
            quality=20,
            optimize=True,
            progressive=True,
        )
        thumb_data = img_io.getvalue()
    except Exception as e:
        print(f"error creating thumbnail for image: {e}")
        # no luck: using placeholder thumbnail
        thumb_data = open("./tinia_manager/static/images/placeholder_thumb.jpg", "rb").read()

    # resize image with PIL, but catch errors
    try:
        img_io = io.BytesIO(img_data)
        img = Image.open(img_io)
        width, height = img.size
        if width > 1024:
            max_size = 1024, 768
            # print(f"resizing image from {width}x{height} to 1024x768")
            img.thumbnail(max_size, Image.BICUBIC)
            img_io = io.BytesIO()
            img.save(
                img_io,
                format="JPEG",
                quality=20,
                optimize=True,
                progressive=True,
            )
            img_data = img_io.getvalue()
    except Exception as e:
        print(f"error resizing image: {e}")
        # no luck: using placeholder image
        img_data = open("./tinia_manager/static/images/placeholder_image.jpg", "rb").read()

    filesaved = False
    maximages = 144
    path_id = images_base + str(webcam.venue_id) + "/"
    p_id = pathlib.Path(path_id)

    # print(f"  saving new images for {webcam.venue.name_ita}")
    if not p_id.exists():
        # print(f"  creating dir {path_id}")
        os.mkdir(path_id)
        with open(path_id + "000.jpg", "wb") as handler:
            handler.write(img_data)
    else:
        lastimgname = zero_pad(maximages - 1)
        lastimgpath = pathlib.Path(path_id + lastimgname + ".jpg")
        # print(f"  lastimgpath = {lastimgpath}")

        for i in range(maximages):
            name = zero_pad(i)
            prename = zero_pad(i - 1)

            imgpath = pathlib.Path(path_id + name + ".jpg")

            if imgpath.is_file() and i != 0 and lastimgpath.is_file():
                # print(f"    renaming {path_id + name}.jpg to ${path_id + prename}.jpg")
                os.rename(path_id + name + ".jpg", path_id + prename + ".jpg")
            elif not imgpath.is_file() and not filesaved:
                imgname = path_id + name + ".jpg"
                # print(f"    saving {imgname}")
                with open(imgname, "wb") as handler:
                    handler.write(img_data)
                filesaved = True

            if i == maximages - 1 and not filesaved:
                imgname = path_id + name + ".jpg"
                # print(f"    saving {imgname}")
                with open(imgname, "wb") as handler:
                    handler.write(img_data)
                filesaved = True
    with open(path_id + "thumb.jpg", "wb") as handler:
        handler.write(thumb_data)

    # print("  end webcam ===", webcam)

    return {"venue_id": webcam.venue_id, "active": active, "new_checksum": new_checksum, "last_checked": last_checked}


async def open_session(loop, webcams, images_base):
    async with aiohttp.ClientSession(
        loop=loop,
    ) as client:
        return await asyncio.gather(*[get_webcam_img(client, webcam, images_base) for webcam in webcams])


@celery_app.task(soft_time_limit=25 * 60, time_limit=30 * 60)
def get_webcams_img():
    # send webcam images in /images/webcams
    images_base = "/var/tmp/webcam/"

    path = images_base
    p = pathlib.Path(path)
    if not p.exists():
        os.mkdir(path)

    webcams = []
    for venue_type in [6, 7]:  # 6 = webcam, 7 = subwebcams
        id_venue_type_webcam = VenueType.objects.get(pk=venue_type)
        for venue in Venue.objects.filter(id_venue_type=id_venue_type_webcam):
            webcam = Webcam.objects.get(venue=venue)
            webcams.append(webcam)

    chunks_of_webcams = list(chunks(webcams, len(webcams) // 10 + 1))

    loop = asyncio.new_event_loop()

    results = []

    for chunk_of_webcams in chunks_of_webcams:
        start_pt = time.process_time()
        start = time.time()
        print("start loop")
        results.append(loop.run_until_complete(open_session(loop, chunk_of_webcams, images_base)))
        end_pt = time.process_time()
        end = time.time()
        elapsed_pt = end_pt - start_pt
        elapsed = end - start
        print(f"end loop, elapsed time = {elapsed}, elapsed process time = {elapsed_pt}")

    flat_results = [i for sub in results for i in sub]

    # update db outside async func
    for result in flat_results:
        venue_id = result.get("venue_id")
        active = result.get("active")
        last_checked = result.get("last_checked")
        new_checksum = result.get("new_checksum")

        # get the record
        webcam = Webcam.objects.get(venue_id=venue_id)

        # update it
        webcam.active = active
        webcam.last_checked = last_checked
        if webcam.checksums is None:
            webcam.checksums = [new_checksum]
            webcam.alive = True
        else:
            webcam.alive = webcam.checksums.count(new_checksum) < MAX_REPEATED_IMG
            # push the new_checksum to the front of the array
            webcam.checksums.insert(0, new_checksum)
            # keep only the MAX_REPEATED_IMG latest checksums
            webcam.checksums = webcam.checksums[:MAX_REPEATED_IMG]

        webcam.save()

    command = [
        "/usr/bin/rsync",
        "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
        "-avz",
        "--mkpath",
        "--delete",
        f"{images_base}",
        f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/images/webcams",
    ]
    # print("command===", command)

    retcode = call(command)
    if retcode != 0:
        error = "rsync failed with code: %d" % retcode
        raise TaskFailure(error)

    # send content of /api/webcams/ to the website as .json
    serializer = WebcamSerializerDynamic(Webcam.objects.all(), many=True)

    filename = "webcams.json"
    with open(filename, "w") as file:
        file.write(json.dumps(serializer.data, cls=UUIDEncoder, indent=2))

    command = [
        "/usr/bin/rsync",
        "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
        "-avz",
        "--mkpath",
        "--delete",
        f"{filename}",
        f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/data/",
    ]
    # print("command===", command)

    retcode = call(command)
    if retcode != 0:
        error = "rsync failed with code: %d" % retcode
        raise TaskFailure(error)


@celery_app.task()
def post_process_radar(id_pseudoradar_bulletin, fname):
    images_base = "/var/tmp/radar/"

    # set this to True to highlight Trento
    debug = False

    pseudoradar_bulletin = PseudoRadarBulletin.objects.get(id=id_pseudoradar_bulletin)
    pseudoradar_bulletin.post_processed = False
    pseudoradar_bulletin.save()

    with transaction.atomic():
        if debug:
            nc = netCDF4.Dataset(fname, mode="a")
        else:
            nc = netCDF4.Dataset(fname)

        crslambert = CRS.from_proj4(
            "+proj=lcc +lat_0=47.5 +lon_0=13.3333333333333 +lat_1=49 +lat_2=46 "
            + "+x_0=400000 +y_0=400000 +ellps=bessel +units=m +no_defs"
        )
        crswgs84 = CRS.from_epsg(4326)  # WGS84 code
        transformer = Transformer.from_crs(crslambert, crswgs84)
        inverseTransformer = Transformer.from_crs(crswgs84, crslambert)

        # desired coverage
        vminLat = 45.634239
        vmaxLat = 47.982
        vminLon = 9.584
        vmaxLon = 13.766

        vmaxE, vmaxN = inverseTransformer.transform(vminLat, vmaxLon)
        vminE, vminN = inverseTransformer.transform(vmaxLat, vminLon)
        easting = nc.variables["x"]
        northing = nc.variables["y"]
        precipitations = nc.variables["RR5"]

        minE = 0
        maxE = 0
        minN = 0
        maxN = 0

        for i in range(len(easting)):
            if easting[i] < vminE:
                minE = i

            if easting[i] < vmaxE:
                maxE = i

        for i in range(len(northing)):
            if northing[i] > vminN:
                minN = i

            if northing[i] > vmaxN:
                maxN = i

        for i in range(14):
            if minE > 0:
                minE += -1

            if maxE < len(easting):
                maxE += 1

            if minN > 0:
                minN += -1

            if maxN < len(northing):
                maxN += 1

        if debug:
            print("DEBUG MODE: ON")
            trentoLat = 46.071322
            trentoLon = 11.120295
            trentoE, trentoN = inverseTransformer.transform(trentoLat, trentoLon)
            trentoE = round(trentoE, -3)
            trentoN = round(trentoN, -3)
            for i in range(len(easting)):
                if easting[i] < trentoE:
                    trentoI = i

            for i in range(len(northing)):
                if northing[i] > trentoN:
                    trentoJ = i
            precipitations[trentoJ, trentoI] = 100

        easting = easting[minE:maxE]
        northing = northing[minN:maxN]

        # Trasform Lambert coordinates into WGS84
        wgs84 = [[] for _ in range(len(easting))]
        for i in range(len(wgs84)):
            eastingi = [easting[i] for _ in range(len(northing))]
            wgs84[i] = transformer.transform(eastingi, northing)

        minLon = 50
        maxLat = 0

        # Find Minimum Longitude and Maximum Latitude
        for i in range(len(wgs84)):
            lat, lon = wgs84[i]
            for j in range(len(wgs84[0][0])):
                if lon[j] < minLon:
                    minLon = lon[j]
                if lat[j] > maxLat:
                    maxLat = lat[j]

        resolutionCoefficient = 70
        imgWidth = 0
        imgHeight = 0

        imgList = []
        # Populate image list with pixel coordinates (round(x),round(y)) and precipitation value (z)
        for i in range(len(wgs84)):
            lat, lon = wgs84[i]
            for j in range(len(wgs84[0][0])):
                z = precipitations[j + minN][i + minE]
                x = (lon[j] - minLon) * resolutionCoefficient
                y = (maxLat - lat[j]) * resolutionCoefficient
                imgList.append((round(x), round(y), z))

                if round(x) > imgWidth:
                    imgWidth = round(x)
                if round(y) > imgHeight:
                    imgHeight = round(y)

        cropxmin = round((vminLon - minLon) * resolutionCoefficient)
        cropxmax = round((vmaxLon - minLon) * resolutionCoefficient)

        cropymin = round((maxLat - vminLat) * resolutionCoefficient)
        cropymax = round((maxLat - vmaxLat) * resolutionCoefficient)

        img = Image.new(mode="RGBA", size=(imgWidth + 1, imgHeight + 1), color=(255, 255, 255, 0))

        limits = [
            0.008,
            0.042,
            0.083,
            0.167,
            0.25,
            0.417,
            0.583,
            0.833,
            1.083,
            1.333,
            1.667,
            2.083,
            2.5,
            3.333,
            4.167,
            5.833,
            7.25,
            8.667,
        ]

        lower_limits = limits
        upper_limits = limits[1:] + [math.inf]

        ranges = list(zip(lower_limits, upper_limits))

        opacity = 175
        colors = [
            (115, 201, 226, opacity),
            (40, 154, 193, opacity),
            (6, 127, 170, opacity),
            (0, 88, 156, opacity),
            (0, 128, 120, opacity),
            (0, 168, 80, opacity),
            (0, 212, 32, opacity),
            (48, 244, 0, opacity),
            (172, 232, 0, opacity),
            (248, 216, 0, opacity),
            (252, 184, 0, opacity),
            (252, 132, 0, opacity),
            (252, 60, 0, opacity),
            (244, 8, 0, opacity),
            (212, 8, 0, opacity),
            (192, 64, 180, opacity),
            (248, 112, 248, opacity),
            (252, 176, 252, opacity),
        ]

        # Coloring image
        for i in range(len(imgList)):
            j = 0
            for r in ranges:
                if imgList[i][2] > r[0] and imgList[i][2] <= r[1]:
                    img.putpixel((imgList[i][0], imgList[i][1]), (colors[j]))
                j += 1

        # (left, upper, right, lower)
        img = img.crop((cropxmin, cropymax, cropxmax, cropymin))

        filesaved = False
        maximages = 49
        p = pathlib.Path(images_base)

        if not p.exists():
            os.mkdir(images_base)
            imgname = "000.png"
            img.save(images_base + imgname)
        else:
            lastimgname = zero_pad(maximages - 1)
            lastimgpath = pathlib.Path(images_base + lastimgname + ".png")

            for i in range(maximages):
                name = zero_pad(i)
                prename = zero_pad(i - 1)

                imgpath = pathlib.Path(images_base + name + ".png")
                if imgpath.is_file() and i != 0 and lastimgpath.is_file():
                    os.rename(images_base + name + ".png", images_base + prename + ".png")
                elif not imgpath.is_file() and not filesaved:
                    imgname = name + ".png"
                    img.save(images_base + imgname)
                    filesaved = True

                if i == maximages - 1 and not filesaved:
                    imgname = name + ".png"
                    img.save(images_base + imgname)
                    filesaved = True

        command = [
            "/usr/bin/rsync",
            "--chmod=Du=rwx,Dg=rx,Do=rx,Fu=rw,Fg=r,Fo=r",
            "-avz",
            "--mkpath",
            "--delete",
            f"{images_base}",
            f"{env('TINIA_WEBSITE_STATIC')}/{mutable_dir}/images/radar",
        ]

        retcode = call(command)
        if retcode != 0:
            error = "rsync failed with code: %d" % retcode
            raise TaskFailure(error)

        pseudoradar_bulletin.post_processed = True
        pseudoradar_bulletin.save()

        if "MIRROR_MANAGER" in os.environ:
            mirror_radar.delay(fname)
        else:
            os.unlink(fname)

    return 1


@celery_app.task()
def mirror_radar(fname):
    url = os.environ["MIRROR_MANAGER"] + "/api/webhooks/pseudoradar/"
    api_key = os.environ["MIRROR_MANAGER_APIKEY"]
    with open(fname, "rb") as f:
        data = f.read()
    response = requests.post(
        url, data=data, headers={"Authorization": f"Api-Key {api_key}", "Content-Type": "application/x-netcdf"}
    )
    os.unlink(fname)
    if response.status_code != 200:
        raise TaskFailure(f"mirror_radar failed with code: {response.status_code} and content: {response.content}")
