from django.contrib import admin

# Register your models here.
from tinia_manager.tinia.models import (
    CurrentData,
    Data,
    DataBulletin,
    DataType,
    SkyCondition,
    Text,
    TextBulletin,
    TimeLayout,
    Venue,
    VenueType,
    Webcam,
)


class VenueAdmin(admin.ModelAdmin):
    ordering = ("name_eng",)
    list_display = ("name_eng", "get_venue_type_description")

    def get_venue_type_description(self, obj):
        return obj.id_venue_type.description

    get_venue_type_description.short_description = "Venue type"


admin.site.register(TimeLayout)
admin.site.register(VenueType)
admin.site.register(Venue, VenueAdmin)
admin.site.register(DataType)
admin.site.register(DataBulletin)
admin.site.register(Data)
admin.site.register(CurrentData)
admin.site.register(TextBulletin)
admin.site.register(Text)
admin.site.register(SkyCondition)
admin.site.register(Webcam)
