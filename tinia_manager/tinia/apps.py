from django.apps import AppConfig


class TiniaConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "tinia_manager.tinia"
