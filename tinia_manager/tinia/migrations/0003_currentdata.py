from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tinia", "0002_populate_reference_tables"),
    ]

    operations = [
        migrations.RunSQL(
            sql="""
                CREATE VIEW currentdata AS
                    WITH d AS (
                        SELECT
                            data.id,
                            data.id_data_type,
                            data_bulletins.reference + MAKE_INTERVAL(days => time_layouts.start_day_offset) + time_layouts.start_time AS start,
                            data_bulletins.reference + MAKE_INTERVAL(days => time_layouts.end_day_offset) + time_layouts.end_time AS end,
                            data.id_venue,
                            data.value,
                            data_bulletins.last_update
                        FROM
                            data_bulletins
                            JOIN data ON data_bulletins.id = data.id_data_bulletin
                            JOIN time_layouts ON data.id_time_layout = time_layouts.id
                        WHERE
                            NOT data_bulletins.draft
                            AND data_bulletins.start > NOW() - INTERVAL '7 days'
                    ),
                    d1 AS (
                        SELECT
                            *,
                            rank() OVER (
                                PARTITION BY start, "end", id_venue
                                ORDER BY last_update DESC
                            )
                        FROM d
                    )
                    SELECT
                        id,
                        id_data_type,
                        start,
                        "end",
                        id_venue,
                        value
                    FROM d1
                    WHERE rank = 1;
            """,
            reverse_sql="DROP VIEW currentdata;",
        )
    ]
