from django.db import migrations

from tinia_manager.tinia.models import (
    Venue,
    VenueType,
    Webcam,
)


def populate_webcams_venues(apps, schema_editor):
    print("========== populate venues with webcam")
    venue_type_webcam = VenueType.objects.get(pk=6)
    venue_type_subwebcam = VenueType.objects.get(pk=7)
    region_tyrol = Venue.objects.get(pk="aadb73d3-bab3-484f-accf-01e19f7fcc04")
    region_south_tyrol = Venue.objects.get(pk="7d2d63a2-5f2c-4b53-841f-22c50ea03768")
    region_trentino = Venue.objects.get(pk="fba93146-7192-4190-adab-605435fdeea1")

    my_data = [
        {
            "id": "63268152-b830-4663-b01b-ef1f40bc8f1d",
            "elevation": 2200,
            "lat": 46.242201,
            "lon": 11.502645,
            "name_deu": "Alpe-Cermis",
            "name_eng": "Alpe-Cermis",
            "name_ita": "Alpe-Cermis",
            "name_lld": "",
            "weight": 2708,
            "id_venue_type": venue_type_webcam,
            "id_region": region_trentino,
        },
        {
            "id": "72eb53f3-2596-4eaa-8f17-6ea6b51f2a8c",
            "elevation": 262,
            "lat": 46.482174,
            "lon": 11.330047,
            "name_deu": "Bozen",
            "name_eng": "Bolzano",
            "name_ita": "Bolzano",
            "name_lld": "",
            "weight": 7419,
            "id_venue_type": venue_type_webcam,
            "id_region": region_south_tyrol,
        },
        {
            "id": "b9f20f9a-1185-4ed6-88b4-0c294c4483bc",
            "elevation": 2570,
            "lat": 47.057116,
            "lon": 10.503974,
            "name_deu": "Furglerblick Bergstation",
            "name_eng": "Furglerblick Bergstation",
            "name_ita": "Furglerblick Bergstation",
            "name_lld": "",
            "weight": 6072,
            "id_venue_type": venue_type_webcam,
            "id_region": region_tyrol,
        },
    ]

    for record in my_data:
        print("  adding", record)
        Venue.objects.create(**record)

    my_data = [
        {
            "id": "85958e10-7ea3-49e8-b72a-48f5ecc9297b",
            "elevation": 2200,
            "lat": 46.242201,
            "lon": 11.502645,
            "name_deu": "Alpe-Cermis",
            "name_eng": "Alpe-Cermis",
            "name_ita": "Alpe-Cermis",
            "name_lld": "",
            "weight": 8817,
            "id_venue_type": venue_type_subwebcam,
            "id_region": Venue.objects.get(pk="63268152-b830-4663-b01b-ef1f40bc8f1d"),
        },
        {
            "id": "3819963f-feb2-4dbc-adab-ad15bfd33fdb",
            "elevation": 276,
            "lat": 46.482174,
            "lon": 11.330047,
            "name_deu": "Bozen",
            "name_eng": "Bolzano",
            "name_ita": "Bolzano",
            "name_lld": "",
            "weight": 1989,
            "id_venue_type": venue_type_subwebcam,
            "id_region": Venue.objects.get(pk="72eb53f3-2596-4eaa-8f17-6ea6b51f2a8c"),
        },
        {
            "id": "7682c058-6a34-4019-8ba8-5083a4e61d64",
            "elevation": 2576,
            "lat": 47.057116,
            "lon": 10.503974,
            "name_deu": "See im Paznaun - FlyingCam",
            "name_eng": "See im Paznaun - FlyingCam",
            "name_ita": "See im Paznaun - FlyingCam",
            "name_lld": "",
            "weight": 8796,
            "id_venue_type": venue_type_subwebcam,
            "id_region": Venue.objects.get(pk="b9f20f9a-1185-4ed6-88b4-0c294c4483bc"),
        },
    ]

    for record in my_data:
        print("  adding", record)
        Venue.objects.create(**record)


def populate_webcam(apps, schema_editor):
    print("========== populate Webcam")
    my_data = [
        {
            "venue": Venue.objects.get(pk="63268152-b830-4663-b01b-ef1f40bc8f1d"),
            "image_url": "https://www.visittrentino.info//var/tmp/dailywebcam/362_%Y_%-m_%-d_%-H.jpg",
            "link": "https://www.visittrentino.info/it/webcams/bassa-val-di-fiemme_w_4143",
        },
        {
            "venue": Venue.objects.get(pk="85958e10-7ea3-49e8-b72a-48f5ecc9297b"),
            "image_url": "https://www.visittrentino.info//var/tmp/dailywebcam/118_%Y_%-m_%-d_%-H.jpg",
            "link": "https://www.visittrentino.info/it/webcams/il-pajon_w_4139",
        },
        {
            "venue": Venue.objects.get(pk="72eb53f3-2596-4eaa-8f17-6ea6b51f2a8c"),
            "link": "https://www.ras.bz.it/de/webcams/bozen/",
            "image_url": "https://www.ras.bz.it/fileadmin/Webcams/sorted/boz/cam1/latest.jpg",
        },
        {
            "venue": Venue.objects.get(pk="3819963f-feb2-4dbc-adab-ad15bfd33fdb"),
            "link": "https://www.ras.bz.it/de/webcams/bozen/",
            "image_url": "https://www.ras.bz.it/fileadmin/Webcams/sorted/enn/cam1/latest.jpg",
        },
        {
            "venue": Venue.objects.get(pk="b9f20f9a-1185-4ed6-88b4-0c294c4483bc"),
            "image_url": "http://wtvpict.feratel.com/picture/38/5557.jpeg?t=38&dcsdesign=WTP_zamgEuregioProjekt&design=v4",
            "link": "http://www.feratel.com/orte/ortid/04782B6E-4C9C-4DEB-BAE7-FBE4D837E091/webcam/oesterreich/tirol/see-im-paznaun.html",
        },
        {
            "venue": Venue.objects.get(pk="7682c058-6a34-4019-8ba8-5083a4e61d64"),
            "image_url": "http://wtvpict.feratel.com/picture/38/75555.jpeg?t=38&dcsdesign=WTP_zamgEuregioProjekt&design=v4",
            "link": "http://www.feratel.com/orte/ortid/04782B6E-4C9C-4DEB-BAE7-FBE4D837E091/webcam/oesterreich/tirol/see-im-paznaun.html",
        },
    ]

    for record in my_data:
        print("  adding", record)
        Webcam.objects.create(**record)


class Migration(migrations.Migration):
    dependencies = [
        ("tinia", "0007_webcam"),
    ]

    operations = [
        migrations.RunPython(populate_webcams_venues),
        migrations.RunPython(populate_webcam),
    ]
