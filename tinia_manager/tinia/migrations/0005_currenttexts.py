from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("tinia", "0004_currentdata"),
    ]

    operations = [
        migrations.RunSQL(
            sql="""
                CREATE VIEW currenttexts AS
                    WITH d AS (
                        SELECT
                            texts.id,
                            text_bulletins.author AS author,
                            text_bulletins.reference + MAKE_INTERVAL(days => time_layouts.start_day_offset) + time_layouts.start_time AS start,
                            text_bulletins.reference + MAKE_INTERVAL(days => time_layouts.end_day_offset) + time_layouts.end_time AS end,
                            texts.id_venue,
                            texts.value_deu,
                            texts.value_eng,
                            texts.value_ita,
                            texts.value_lld,
                            text_bulletins.last_update
                        FROM
                            text_bulletins
                            JOIN texts ON text_bulletins.id = texts.id_text_bulletin
                            JOIN time_layouts ON texts.id_time_layout = time_layouts.id
                        WHERE
                        NOT text_bulletins.draft
                        AND text_bulletins.start > NOW() - INTERVAL '5 days'
                    ),
                    d1 AS (
                        SELECT
                            *,
                            rank() OVER (
                                PARTITION BY start, "end", id_venue
                                ORDER BY last_update DESC
                            )
                        FROM d
                    )
                    SELECT
                        id,
                        author,
                        start,
                        "end",
                        id_venue,
                        value_deu,
                        value_eng,
                        value_ita,
                        value_lld,
                        last_update
                    FROM d1
                    WHERE rank = 1;
            """,
            reverse_sql="DROP VIEW currenttexts;",
        )
    ]
