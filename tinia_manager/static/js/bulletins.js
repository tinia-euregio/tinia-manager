window.addEventListener("pageshow", function (event) {
  var historyTraversal = event.persisted ||
    (typeof window.performance != "undefined" &&
      window.performance.navigation.type === 2);
  if (historyTraversal) {
    // Handle page restore.
    window.location.reload();
  }
});

const startYear = 2022;

document.addEventListener('alpine:init', () => {
  Alpine.data('bulletinData', () => ({
    alldata: [],
    bulletins: [],
    active_ids: [],
    lastupdate_ids: [],
    regions: null,
    regionLength: 0,
    sortCol: null,
    sortAsc: false,
    curPage: 1,
    totalPage: null,
    pageArray: [],
    ready: false,
    years: [...Array((new Date()).getFullYear() - startYear + 1).keys()].map(x => x + startYear).reverse(),
    month: 0,
    year: (new Date()).getFullYear(),
    async loadData() {
      const month = document.getElementById('monthnum').value
      const year = document.getElementById('yearnum').value
      const isDraft = document.getElementById('draft_boolean').value
      const region = document.getElementById('region_filter').value
      const sortCol = this.sortCol
      const sortAsc = this.sortAsc
      const page = this.curPage
      const isDataPage = document.getElementById('data-page') ? 1 : 0;

      const payload = {
        month,
        year,
        isDraft,
        region,
        sortCol,
        sortAsc,
        page,
        isDataPage
      }
      const response = await fetch('/home?' + new URLSearchParams(payload));
      const responseJson = await response.json();
      this.regions = responseJson["regions"];
      this.regionLength = this.regions.length;
      this.bulletins = responseJson["bulletins"];
      this.totalPage = responseJson["totalpages"]
      this.active_ids = responseJson["active_ids"]
      this.lastupdate_ids = responseJson["lastupdate_ids"]
      this.ready = true
    },
    async init() {
      await this.loadData()
    },
    nextPage() {
      if (this.curPage < this.totalPage) {
        this.curPage++;
        this.ready = false;
        this.loadData()
      }
    },
    clickedPage(page) {
      if (page != this.curPage) {
        this.curPage = page;
        this.ready = false;
        this.loadData()
      }
    },
    previousPage() {
      if (this.curPage > 1) {
        this.curPage--;
        this.ready = false;
        this.loadData()
      }
    },
    clearLocalStorage() {
      localStorage.setItem("activeTab", '')
    },
    sort(col) {
      if (this.sortCol === col) this.sortAsc = !this.sortAsc;
      else this.sortAsc = false;
      this.sortCol = col;
      this.curPage = 1;
      this.ready = false;
      this.loadData()
      if (this.sortCol !== 'month' && this.sortCol !== 'year' && this.sortCol !== 'draft_boolean' && this.sortCol !== 'region_filter') {
        if (this.sortAsc) document.getElementById('sort_' + this.sortCol).innerHTML = '<i class="bi bi-sort-up"></i>'
        else document.getElementById('sort_' + col).innerHTML = '<i class="bi bi-sort-down"></i>'
      }
      this.removeSortIcon(col)
    },
    removeSortIcon(col) {
      let sortArray = []
      if (document.getElementById('data_num')) {
        sortArray = ['id', 'start', 'end', 'last_update', 'author', 'version', 'draft', 'id_venue_id', 'data_num']
      }
      else {
        sortArray = ['id', 'start', 'end', 'last_update', 'author', 'version', 'draft', 'id_venue_id']
      }
      for (let i = 0; i < sortArray.length; i++) {
        if (sortArray[i] != col) { document.getElementById('sort_' + sortArray[i]).innerHTML = '' }
      }
    },
    get pagedbulletins() {
      return this.bulletins
    }
  }))
});

function confirmAction(editable_status, item_id) { // eslint-disable-line no-unused-vars
  if (editable_status == "1") {
    let confirmBoolean = confirm("Are you sure to execute this action?");
    if (confirmBoolean) {
      alert("Action successfully executed");
      window.location.href = "delete/" + item_id
    } else {
      alert("Action canceled");
    }
  } else {
    alert("Can't delete this bulletin");
  }
}

function range(n) {
  return Array.apply(null, Array(n)).map(function (_, i) { return i; }
  );
}

function generatePagination(currentPage, totalPages) { // eslint-disable-line no-unused-vars

  // First we move out the configurations. That way, they don't mingle with the logic.
  var initialChunkPadding = 1;
  var middleChunkPadding = 2;
  var endingChunkPadding = 1;
  var gapValue = '...';

  // Instead of a loop, we use range. It's much cleaner and we don't have tracking variables
  // at the cost of generating an array.
  return range(totalPages).reduce(function (pagination, index) {

    // Then we determine what the current page is based on some comparisons
    var page = index + 1;
    var isInitialChunk = page <= 1 + initialChunkPadding;
    var isMiddleChunk = page >= currentPage - middleChunkPadding && page <= currentPage + middleChunkPadding;
    var isEndingChunk = page >= totalPages - endingChunkPadding;
    var hasNoGap = pagination[pagination.length - 1] !== gapValue;

    // Then based on the determinations, we determine what value gets pushed into the array.
    // It can either be the page, a '...', or a blank array (which doesn't change anything with concat)
    var valueToAdd = isInitialChunk || isMiddleChunk || isEndingChunk ? page : hasNoGap ? gapValue : [];

    return pagination.concat(valueToAdd);
  }, []);
}
