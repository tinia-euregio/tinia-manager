window.addEventListener("pageshow", function (event) {
  var historyTraversal = event.persisted ||
    (typeof window.performance != "undefined" &&
      window.performance.navigation.type === 2);
  if (historyTraversal) {
    // Handle page restore.
    window.location.reload();
  }
});

document.addEventListener('alpine:init', () => {
  Alpine.data('bulletins', () => ({
    bulletin_type: 'Pseudoradar',
    bulletin_description: 'Bulletins with NetCDF file attached',
    results: [],
    count: 0,
    next: null,
    previous: null,
    curPage: 1,
    totalPage: 0,
    ready: false,
    async loadData() {
      const page = this.curPage

      const response = await fetch(`/api/pseudoradar_bulletins/?page=${this.curPage}`);
      const responseJson = await response.json();
      this.results = responseJson["results"];
      this.count = responseJson["count"]
      this.totalPage = Math.ceil(this.count / 10)
      this.next = responseJson["next"]
      this.previous = responseJson["previous"]
      this.ready = true
    },
    async init() {
      await this.loadData()
    },
    nextPage() {
      this.curPage++;
      this.ready = false;
      this.loadData()
    },
    clickedPage(page) {
      this.curPage = page;
      this.ready = false;
      this.loadData()
    },
    previousPage() {
      this.curPage--;
      this.ready = false;
      this.loadData()
    },
  }))
});

function range(n) {
  return Array.apply(null, Array(n)).map(function (_, i) { return i; }
  );
}

function generatePagination(currentPage, totalPages) { // eslint-disable-line no-unused-vars

  // First we move out the configurations. That way, they don't mingle with the logic.
  var initialChunkPadding = 1;
  var middleChunkPadding = 2;
  var endingChunkPadding = 1;
  var gapValue = '...';

  // Instead of a loop, we use range. It's much cleaner and we don't have tracking variables
  // at the cost of generating an array.
  return range(totalPages).reduce(function (pagination, index) {

    // Then we determine what the current page is based on some comparisons
    var page = index + 1;
    var isInitialChunk = page <= 1 + initialChunkPadding;
    var isMiddleChunk = page >= currentPage - middleChunkPadding && page <= currentPage + middleChunkPadding;
    var isEndingChunk = page >= totalPages - endingChunkPadding;
    var hasNoGap = pagination[pagination.length - 1] !== gapValue;

    // Then based on the determinations, we determine what value gets pushed into the array.
    // It can either be the page, a '...', or a blank array (which doesn't change anything with concat)
    var valueToAdd = isInitialChunk || isMiddleChunk || isEndingChunk ? page : hasNoGap ? gapValue : [];

    return pagination.concat(valueToAdd);
  }, []);
}
