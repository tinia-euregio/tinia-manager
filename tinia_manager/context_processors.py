import os


def export_vars(request):
    data = {}
    data["ADMIN_URL"] = os.environ.get("DJANGO_ADMIN_URL", "admin/")
    return data
