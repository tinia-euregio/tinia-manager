from django.contrib.auth import forms as admin_forms
from django.contrib.auth import get_user_model
from django.utils.translation import gettext_lazy as _

User = get_user_model()
REGION_CHOICES = (
    ("aadb73d3-bab3-484f-accf-01e19f7fcc04", "Tyrol"),
    ("7d2d63a2-5f2c-4b53-841f-22c50ea03768", "South Tyrol"),
    ("fba93146-7192-4190-adab-605435fdeea1", "Trentino"),
)


class UserAdminChangeForm(admin_forms.UserChangeForm):
    class Meta(admin_forms.UserChangeForm.Meta):
        model = User

    def __init__(self, *args, **kwargs):
        super(UserAdminChangeForm, self).__init__(*args, **kwargs)
        qs = self.fields["region"].queryset
        self.fields["region"].queryset = qs.filter(id_venue_type_id=0).order_by("name_eng")


class UserAdminCreationForm(admin_forms.UserCreationForm):
    """
    Form for User Creation in the Admin Area.
    To change user signup, see UserSignupForm and UserSocialSignupForm.
    """

    class Meta(admin_forms.UserCreationForm.Meta):
        model = User

        error_messages = {"username": {"unique": _("This username has already been taken.")}}
