module.exports = {
  root: true,
  env: {
    browser: true,
    es2017: true
  },
  plugins: [
    "html",
    "@eladavron/jinja"
  ],
  extends: [
    'eslint:recommended',
  ],
  globals: {
    Alpine: "readonly"
  },
}
