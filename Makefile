all:
	yarnpkg
	cp node_modules/bootstrap/dist/css/bootstrap.min.css tinia_manager/static/css/.
	cp node_modules/bootstrap/dist/css/bootstrap.min.css.map tinia_manager/static/css/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js tinia_manager/static/js/.
	cp node_modules/bootstrap/dist/js/bootstrap.min.js.map tinia_manager/static/js/.
	cp node_modules/@popperjs/core/dist/umd/popper.min.js tinia_manager/static/js/.
	cp node_modules/@popperjs/core/dist/umd/popper.min.js.map tinia_manager/static/js/.
	cp node_modules/bootstrap-icons/font/bootstrap-icons.css tinia_manager/static/css/.

	mkdir -p tinia_manager/static/css/fonts
	cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff tinia_manager/static/css/fonts/.
	cp node_modules/bootstrap-icons/font/fonts/bootstrap-icons.woff2 tinia_manager/static/css/fonts/.

	mkdir -p tinia_manager/static/icons
	cp node_modules/bootstrap-icons/icons/eye.svg tinia_manager/static/icons/.
	cp node_modules/bootstrap-icons/icons/trash3.svg tinia_manager/static/icons/.
	cp node_modules/bootstrap-icons/icons/arrow-clockwise.svg tinia_manager/static/icons/.
	cp node_modules/alpinejs/dist/cdn.min.js tinia_manager/static/js/alpine.js
	cp node_modules/netlify-cms/dist/netlify-cms.js* tinia_manager/static/js/.
