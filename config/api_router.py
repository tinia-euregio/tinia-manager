from django.conf import settings
from rest_framework.routers import DefaultRouter, SimpleRouter

from tinia_manager.tinia.views import (
    CurrentTextsView,
    DataBulletinView,
    DataTypeView,
    DataView,
    PreviTempView,
    PseudoRadarBulletinView,
    SkyConditionView,
    TimeLayoutView,
    VenueTypeView,
    VenueView,
    WebcamView,
)
from tinia_manager.users.api.views import UserViewSet

if settings.DEBUG:
    router = DefaultRouter()
else:
    router = SimpleRouter()

router.register("users", UserViewSet)
router.register("data", DataView)
router.register("schemas", DataTypeView)
router.register("sky_conditions", SkyConditionView)
router.register("venues", VenueView)
router.register("venue_types", VenueTypeView)
router.register("webcams", WebcamView)
# router.register("currentdata", CurrentDataView)
router.register("previ_temp", PreviTempView)
router.register("currenttexts", CurrentTextsView)
router.register("time_layouts", TimeLayoutView)
router.register("data_bulletins", DataBulletinView)
router.register("pseudoradar_bulletins", PseudoRadarBulletinView)

app_name = "api"
urlpatterns = router.urls
