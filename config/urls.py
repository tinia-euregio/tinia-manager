from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path
from django.views import defaults as default_views
from django.views.generic import TemplateView
from drf_spectacular.views import SpectacularAPIView, SpectacularSwaggerView
from rest_framework.authtoken.views import obtain_auth_token

from tinia_manager.tinia import views

urlpatterns = [
    # path("", views.index, name="index"),
    path(
        "",
        views.IndexPageView.as_view(template_name="pages/bulletin_texts.html"),
        name="bulletin_texts",
    ),
    path("home", views.home, name="homedata"),
    path("translation", views.translation, name="translation"),
    path(
        "create/texts",
        views.BulletinCreate.as_view(template_name="pages/bulletinview.html"),
        name="bulletin_create",
    ),
    path(
        "update/complete",
        views.bulletinTextUpdate,
        name="bulletin_update_complete",
    ),
    path(
        "delete/<str:pk>",
        views.bulletinDelete,
        name="bulletin_delete",
    ),
    path(
        "view/<str:pk>",
        views.BulletinDetailView.as_view(template_name="pages/bulletinview.html"),
        name="bulletin_view",
    ),
    path("send/", views.sendView, name="send"),
    path(
        "bulletindata/",
        views.BulletinDataDetail.as_view(template_name="pages/bulletin_data.html"),
        name="bulletin_data",
    ),
    path(
        "bulletinpseudoradar/",
        views.PseudoRadarBulletinPageView.as_view(),
        name="bulletin_pseudoradar",
    ),
    path(
        "dashboard/",
        views.DashboardPageView.as_view(template_name="pages/dashboard.html"),
        name="dashboard",
    ),
    path(
        "user_regions/",
        views.ManageRegion.as_view(template_name="pages/user_region.html"),
        name="user_region",
    ),
    path(
        "user_region/<int:id>",
        views.RegionSelect.as_view(template_name="pages/region_select.html"),
        name="region_select",
    ),
    path("user_region_save", views.userRegionSave, name="user_region_save"),
    path("about/", TemplateView.as_view(template_name="pages/about.html"), name="about"),
    path("cms/", views.CmsPageView.as_view(), name="cms"),
    path(
        "cms/config.yml",
        TemplateView.as_view(template_name="pages/config.yml", content_type="text/yaml"),
        name="cms-config",
    ),
    path("gitlab.com/<path:path>", views.GitlabProxyView.as_view(), name="gitlab"),
    # Django Admin, use {% url 'admin:index' %}
    path(settings.ADMIN_URL, admin.site.urls),
    # User management
    path("users/", include("tinia_manager.users.urls", namespace="users")),
    path("accounts/", include("allauth.urls")),
    # https://django-invitations.readthedocs.io/en/latest/installation.html#requirements
    path("invitations/", include("invitations.urls", namespace="invitations")),
    # Your stuff: custom urls includes go here
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# API URLS
urlpatterns += [
    # API base url
    path("api/", include("config.api_router")),
    # API webhooks
    path("api/webhooks/forecasts/", views.forecasts_webhook, name="forecast_webhook"),
    path(
        "api/webhooks/observations/",
        views.observations_webhook,
        name="observation_webhook",
    ),
    path("api/webhooks/texts/", views.texts_webhook, name="texts_webhook"),
    path(
        "api/webhooks/pseudoradar/",
        views.pseudoradar_webhook,
        name="pseudoradar_webhook",
    ),
    path("api/webhooks/models/", views.models_webhook, name="models_webhook"),
    # DRF auth token
    path("auth-token/", obtain_auth_token),
    path("api/schema/", SpectacularAPIView.as_view(), name="api-schema"),
    path(
        "api/docs/",
        SpectacularSwaggerView.as_view(url_name="api-schema"),
        name="api-docs",
    ),
]

if settings.DEBUG:
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.
    urlpatterns += [
        path(
            "400/",
            default_views.bad_request,
            kwargs={"exception": Exception("Bad Request!")},
        ),
        path(
            "403/",
            default_views.permission_denied,
            kwargs={"exception": Exception("Permission Denied")},
        ),
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
        path("500/", default_views.server_error),
    ]
    if "debug_toolbar" in settings.INSTALLED_APPS:
        import debug_toolbar

        urlpatterns = [path("__debug__/", include(debug_toolbar.urls))] + urlpatterns
