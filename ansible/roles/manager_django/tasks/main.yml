---

- name: make sure the /srv/manager directory exists
  file:
    path: /srv/manager
    state: directory
    owner: root
    group: manager
    mode: 0755

- name: make sure the /srv/manager/requirements directory exists
  file:
    path: /srv/manager/requirements
    state: directory
    owner: root
    group: manager
    mode: 0755

- name: make sure the /run/celery directory exists
  file:
    path: /run/celery
    state: directory
    owner: manager
    group: manager
    mode: 0755

- name: make sure the /var/log/celery directory exists
  file:
    path: /var/log/celery
    state: directory
    owner: manager
    group: manager
    mode: 0755

- name: install manager_django dependencies
  apt:
    pkg: ['python3-venv', 'build-essential', 'libpq-dev', 'libpython3-dev', 'redis', 'openssh-client', 'rsync']
    state: present
    update_cache: true

- name: uninstall firewall
  apt:
    pkg: ['ufw']
    state: absent
    update_cache: false

- name: install the manager systemd unit file
  ansible.builtin.copy: src=manager.service dest=/etc/systemd/system/manager.service
        owner=root
        group=root
        mode=0644

- name: install the celery systemd unit file
  ansible.builtin.copy: src=celery.service dest=/etc/systemd/system/celery.service
        owner=root
        group=root
        mode=0644

- name: install the celerybeat systemd unit file
  ansible.builtin.copy: src=celerybeat.service dest=/etc/systemd/system/celerybeat.service
        owner=root
        group=root
        mode=0644

- name: reload systemd
  command: systemctl --system daemon-reload

- name: stop manager service
  command: systemctl stop manager

- name: stop celerybeat service
  command: systemctl stop manager

- name: push the code (environment)
  ansible.builtin.copy:
    src: ../../../../.envs/.sandbox/.django
    dest: /srv/manager/.env
    owner: root
    group: root

- name: push the code (base requirements)
  ansible.builtin.copy:
    src: ../../../../requirements/base.txt
    dest: /srv/manager/requirements/base.txt
    owner: root
    group: root

- name: push the code (production requirements)
  ansible.builtin.copy:
    src: ../../../../requirements/production.txt
    dest: /srv/manager/requirements/production.txt
    owner: root
    group: root

- name: push the code (manage.py script)
  ansible.builtin.copy:
    src: ../../../../manage.py
    dest: /srv/manager/manage.py
    owner: manager
    group: manager

- name: push the code (manager app)
  synchronize:
    src: ../../../../tinia_manager/
    dest: /srv/manager/tinia_manager

- name: push the code (config dir)
  synchronize:
    src: ../../../../config/
    dest: /srv/manager/config

- name: push the code (lorem files)
  ansible.builtin.copy:
    src: "{{ item }}"
    dest: /srv/manager
  with_fileglob:
    - "../../../../lorem_*"

- name: push the code (test JSONs)
  ansible.builtin.copy:
    src: "{{ item }}"
    dest: /srv/manager/test/
  with_fileglob:
    - "../../../../test/*.json"

- pip:
    requirements: /srv/manager/requirements/production.txt
    virtualenv: /srv/manager/venv
    virtualenv_command: /usr/bin/python3 -m venv

- name: make sure the /srv/manager/staticfiles directory exists
  file:
    path: /srv/manager/staticfiles
    state: directory
    owner: manager
    group: manager
    mode: 0755

- name: collect static assets, migrate
  ansible.builtin.shell:
    cmd: |
      source ./venv/bin/activate
      set -a
      . ./.env
      python3 manage.py collectstatic --noinput
      python3 manage.py migrate
    chdir: /srv/manager/
  args:
    executable: /bin/bash

- name: start manager service
  command: systemctl start manager

- name: ensure that manager is started
  service: name=manager
           state=started
           enabled=yes

- name: start celery service
  command: systemctl start celery

- name: start celerybeat service
  command: systemctl start celerybeat

- name: ensure that celerybeat is started
  service: name=celerybeat
           state=started
           enabled=yes
