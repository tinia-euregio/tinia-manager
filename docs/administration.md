# System administration guide for the meteo.report Manager

How to operate the meteo.report Manager.

[[_TOC_]]

## Setting Up Your Users

- To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go check you email (or in development mode go to your console to see a simulated email verification message). Copy the link into your browser. Now the user's email should be verified and ready to go.

- To create a **superuser account**, use this command [inside the django service](https://docs.docker.com/compose/reference/exec/):

      docker-compose -f local.yml run --rm django python manage.py createsuperuser --email root@example.com --username root

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

## Downloading data

The API provides several endpoints for downloading data in JSON format, both **reference data**:

- [http://localhost:8000/api/sky_conditions/](http://localhost:8000/api/sky_conditions/): the list of sky condition icons

- [http://localhost:8000/api/venues/](http://localhost:8000/api/venues/): the list of locations e.g. points on the earth, cities or political/non-political geographical areas

- [http://localhost:8000/api/time_layouts/](http://localhost:8000/api/time_layouts/) reference time layouts for the data validity time ranges; the view can be filtered:

  - by `start_day_offset`: [http://localhost:8000/api/time_layouts/?start_day_offset=4](http://localhost:8000/api/time_layouts/?start_day_offset=4)

  - by `end_day_offset`: [http://localhost:8000/api/time_layouts/?end_day_offset=-6](http://localhost:8000/api/time_layouts/?end_day_offset=-6)

  - by duration in `seconds`: [http://localhost:8000/api/time_layouts/?seconds=86400](http://localhost:8000/api/time_layouts/?seconds=86400)
  
  - by time range: [http://localhost:8000/api/time_layouts/?start=05:00&end=07:00](http://localhost:8000/api/time_layouts/?start=05:00&end=07:00)

as well as **live data**:

N.B.: `currentdata` endpoint is currently disabled, to view it at the link below it must be uncommented and imported in `/tinia-manager/config/api_router.py`

- [http://localhost:8000/api/currentdata/](http://localhost:8000/api/currentdata/): the currently valid forecasts/observations for all venues and all time layouts; the view can be filtered:
  
  - by venue: [http://127.0.0.1:8000/api/currentdata/?id_venue=00007b25-3061-4b73-99ad-a1a65f76fa95](http://127.0.0.1:8000/api/currentdata/?id_venue=00007b25-3061-4b73-99ad-a1a65f76fa95)

  - by forecasts: [http://127.0.0.1:8000/api/currentdata/?id_data_type=forecast_record_schema](http://127.0.0.1:8000/api/currentdata/?id_data_type=forecast_record_schema)

  - by observations: [http://127.0.0.1:8000/api/currentdata/?id_data_type=observation_record_schema](http://127.0.0.1:8000/api/currentdata/?id_data_type=observation_record_schema)

  - for records past a certain datetime: [http://127.0.0.1:8000/api/currentdata/?start=2022-05-23T21:00:00](http://127.0.0.1:8000/api/currentdata/?start=2022-05-23T21:00:00)

  - by datetime range: [http://127.0.0.1:8000/api/currentdata/?end=2022-05-20T03:00:00%2B02:00&start=2022-05-20T00:00:00%2B02:00](http://127.0.0.1:8000/api/currentdata/?end=2022-05-20T03:00:00%2B02:00&start=2022-05-20T00:00:00%2B02:00) (note the `+` chaaracter in the timezone must be escaped as `%2B`)

Alternatively you can download all the reference:

    curl -s http://127.0.0.1:8000/api/sky_conditions/ | python3 -m json.tool > sky_conditions.json
    curl -s http://127.0.0.1:8000/api/venues/ | python3 -m json.tool > venues.json
    curl -s http://127.0.0.1:8000/api/time_layouts/ | python3 -m json.tool > time_layouts.json

and live data at once:

    curl -s http://127.0.0.1:8000/api/currenttexts/ | python3 -m json.tool > bulletins.json
    curl -s http://127.0.0.1:8000/api/currentdata/?id_data_type=forecast_record_schema | python3 -m json.tool > forecasts.json  # ~16 MB
    curl -s http://127.0.0.1:8000/api/currentdata/?id_data_type=observation_record_schema | python3 -m json.tool > observations.json  # ~100 MB

## Generating API keys

API keys are used to give access to certain API endpoints (the web-hooks) to external projects and scripts.

meteo.report Manager uses the [Django REST Framework API Key plugin](https://florimondmanca.github.io/djangorestframework-api-key/guide/) to create and manage API keys.

In the "_API Key Permissions_" section ([http://localhost:8000/admin/rest_framework_api_key/apikey/](http://localhost:8000/admin/rest_framework_api_key/apikey/)) of the Django admin site you can create, view and revoke API keys.

## Uploading data

Once you have an API key, you can upload data in JSON format to the web-hooks; for example to send the sample file `test/forecast_message.json` which contains random forecast data (replace `xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy` with your secret API key):

    curl --fail-with-body -i -H "Authorization: Api-Key xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" -X POST -H 'Content-Type: application/json' -d @test/forecast_message.json http://localhost:8000/api/webhooks/forecasts/

or to send the sample file `test/observation_message.json` which contains random observation data:

    curl --fail-with-body -i -H "Authorization: Api-Key xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" -X POST -H 'Content-Type: application/json' -d @test/observation_message.json http://localhost:8000/api/webhooks/observations/

or to send the sample file `test/texts_message.json` which contains random texts:

    curl --fail-with-body -i -H "Authorization: Api-Key xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" -X POST -H 'Content-Type: application/json' -d @test/texts_message.json http://localhost:8000/api/webhooks/texts/

or to send netcdf pseudoradar:

    curl --fail-with-body -i -H "Authorization: Api-Key xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" -X POST -H 'Content-Type: application/x-netcdf' --data-binary @x.nc http://localhost:8000/api/webhooks/pseudoradar/

or to send raster data for COSMO-1E model forecasts (as a ZIP file containing `bulletin.json` and the `tot_prec_01h` raster images for all `instant` time layouts):

    curl --fail-with-body -i -H "Authorization: Api-Key xxxxxxxx.yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy" -X POST -H 'Content-Type: application/zip' --data-binary @output.zip http://localhost:8000/api/webhooks/models/

The options are:

- `--fail-with-body `: forces curl to return an error code if there was a problem retrieving the HTTP resource (useful to catch 400, 403 and 404 error when the command is run unattended) while allow curl to output and save the content.
- `-i` (`--include`): includes the HTTP response headers in the output for debugging purposes
- `-H` (`--header`): adds additional headers to the request; we add the `Authorization` header with the API key and the `Content-Type` one to signal the server that the data will be sent in JSON format
- `-X` (`--request`): sets the HTTP verb to `POST`
- `-d` (`--data`) followed by `@` will send the specified file to the HTTP server as body of the request.
- `--data-binary` followed by `@` will posts data exactly as specified with no extra processing whatsoever.

Prepare your data in accordance with the supplied JSON schemas (`test/forecast_message_schema.json`, `test/observation_message_schema.json` and `test/texts_message_schema.json`).

The currently valid schemas can also be downloaded from the manager itself:

- [https://manager.meteo.report/api/schemas/forecast_message_schema/](https://manager.meteo.report/api/schemas/forecast_message_schema/)
- [https://manager.meteo.report/api/schemas/observation_message_schema/](https://manager.meteo.report/api/schemas/observation_message_schema/)
- [https://manager.meteo.report/api/schemas/texts_message_schema/](https://manager.meteo.report/api/schemas/texts_message_schema/)

or from the sandbox:

- [https://tinia-manager.simevo.com/api/schemas/forecast_message_schema/](https://tinia-manager.simevo.com/api/schemas/forecast_message_schema/)
- [https://tinia-manager.simevo.com/api/schemas/observation_message_schema/](https://tinia-manager.simevo.com/api/schemas/observation_message_schema/)
- [https://tinia-manager.simevo.com/api/schemas/texts_message_schema/](https://tinia-manager.simevo.com/api/schemas/texts_message_schema/)

You can use the `test/validate.py` Python script to make sure your data is valid before trying to send it, for example to validate the sample files:

    ./test/validate.py -s test/forecast_message_schema test/forecast_message.json
    ./test/validate.py -s test/observation_message_schema test/observation_message.json
    ./test/validate.py -s test/forecast_message_schema test/forecast_message.json

or to validate your own message:

    ./test/validate.py -s test/observation_message measurement_data_tyrol.json

## Forcing an asynchronous job

To manually force the execution of an asynchronous job in production:

    mosh manager@manager.meteo.report
    cd /srv/manager/
    set -a
    . ./.env
    source ./venv/bin/activate
    python3 manage.py shell
    from tinia_manager.tinia.tasks import post_process_text_bulletin
    post_process_text_bulletin(1212)  # assuming 1212 is the id_text_bulletin
    ^d
    deactivate
    ^d

and in development:

    docker-compose -f local.yml run --rm celeryworker python manage.py shell
    from tinia_manager.tinia.tasks import post_process_forecast_bulletin
    post_process_forecast_bulletin(1313)  # assuming 1313 is the id_data_bulletin
    ^d

## Schedule a recurring asynchronous job

To schedule a recurring asynchronous job, for example updating texts on meteo.report website when the UTC calendar day changes:

1. Access the Django administration site as `root` user.

2. Create a crontab schedule at: `1 0 * * * (m/h/dM/MY/d) UTC` on https://manager.meteo.report/admin/django_celery_beat/crontabschedule/

3. Create a periodic task that executes the registered task (the task must be callable without suppling args!) `tinia_manager.tinia.tasks.post_process_texts` with the Crontab Schedule defined above at https://manager.meteo.report/admin/django_celery_beat/periodictask/.

## Monitoring the ansynchronous jobs

Access the [Flower](https://flower.readthedocs.io/en/latest/) dashboard at [http://localhost:5555/](http://localhost:5555/) to monitor the asnychronous jobs.
