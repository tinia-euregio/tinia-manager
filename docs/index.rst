.. meteo.report Manager documentation master file, created by
   sphinx-quickstart.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to meteo.report Manager's documentation!
======================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   howto
   users
   administration.md
   development.md
   data_models.md



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
