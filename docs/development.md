# Developer Guide

The meteo.report Manager is a server-side-rendered web application implemented in the Python 3.9 language with the [Django web framework](https://www.djangoproject.com/).

It also includes:

- a [PostgreSQL](https://www.postgresql.org/) relational database

- an API with web-hooks for programmatical uploads, based on [Django REST framework (DRF)](https://www.django-rest-framework.org/)

- the [Celery](https://docs.celeryproject.org/en/stable/getting-started/introduction.html) scheduler to execute long-running or event-based jobs 

All Python code follows the [Black code style](https://github.com/ambv/black), the PEP8 Style Guide as per [flake8](https://github.com/pycqa/flake8), and import ordering as per [isort](https://github.com/PyCQA/isort).

The project was initialy generated with [Cookiecutter Django](https://github.com/pydanny/cookiecutter-django/) (see the initial commit) but it is now manually managed.

[[_TOC_]]

## Settings

See the [Cookiecutter Django settings reference](http://cookiecutter-django.readthedocs.io/en/latest/settings.html)

## Prerequisites

Tested on Debian bookworm (12) and Ubuntu jammy (22.04 LTS) with these packages installed:

    apt install docker docker-compose git make yarnpkg curl

clone this repo with HTTPS:

    git clone https://gitlab.com/tinia-euregio/tinia-manager

or with SSH if you have configured your gitlab account with [ssh keys](https://gitlab.com/-/profile/keys):

    git clone git@gitlab.com:tinia-euregio/tinia-manager.git

Download and install the JavaScript dependencies with:

    make

## Basic Commands

### Starting the web-app

To start the web-app in development mode, run:

    docker-compose -f local.yml up

Now you can:

- Open the web-app: [http://localhost:8000](http://localhost:8000)

- Open the Django REST framework API Root: [http://localhost:8000/api/](http://localhost:8000/api/) or better yet the interactive API documentation: [http://localhost:8000/api/docs/](http://localhost:8000/api/docs/) (use superuser or staff user credentials)

- Open the Django administration Site (use superuser or staff user credentials): [http://localhost:8000/admin/](http://localhost:8000/admin/)

- Open the project documentation: [http://localhost:9000/](http://localhost:9000/)

- Browse the database schema and tables with [Adminer](https://www.adminer.org/): [http://localhost:8090](http://localhost:8090/?pgsql=postgres&username=CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV&db=tinia_manager&ns=public) (use credentials from `.envs/.local/.postgres`)

- Access the PostgreSQL database with `PGPASSWORD=IU2Cxkck4VF9mibp7EBN4drslft7PG0ApYxbW7RPVbCfBSJRqulezsyLUE0yu5Qg psql -p 5433 -h localhost -U CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV tinia_manager`.

- Access the [Flower](https://flower.readthedocs.io/en/latest/) dashboard at [http://localhost:5555/](http://localhost:5555/) with the credentials `CELERY_FLOWER_USER` / `CELERY_FLOWER_PASSWORD` from `.envs/.local/.django` to monitor the asnychronous jobs.

For more information on creating users and in general how to operate the meteo.report Manager, read the [system administration guide](./administration.md).

### Type checks

Running type checks with mypy:

    docker-compose -f local.yml run --rm django mypy tinia_manager

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    docker-compose -f local.yml run --rm django coverage run -m pytest
    docker-compose -f local.yml run --rm django coverage html
    open htmlcov/index.html

#### Running tests with pytest

    docker-compose -f local.yml run --rm django pytest

### Live reloading and Sass CSS compilation

See [Live reloading and SASS compilation](http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html).

### Celery

This app comes with Celery.

To run a celery worker:

``` bash
cd tinia_manager
celery -A config.celery_app worker -l info
```

Please note: For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with *manage.py*, you should be right.

### Testing the web-hooks

The web-hooks are special API endpoints used to upload texts and data programmatically; upon receipt they:

1. validate the supplied data based on JSON schemas for the different messages

2. _[optionally]_ break up the supplied data in separate records associated to a single venue / time layout combination

3. store the received records in the DB

4. schedule additional processing to be executed asynchronously by a Celery task

5. on success, return a confirmation message with a **200** [HTTP status code](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes), else an error code such as:

   - **400** = Bad Request, typically when the supplied data is malformed)
   - **403** = Forbidden, typically when the API key is missing or invalid
   - or **404** = Not Found, typically when the wrong endpoint is specified.

The asynchronous postprocessing involves extracting all the currently valid records (for all texts when a text message is received, and for all data when an observation or forecast data message is received) and updating the static data in the meteo.report website.

For the sandbox and production environments, meteo.report is the live instance running at https://tinia.simevo.com or https://meteo.report respectively, so the files must be uploaded via rsync to the appropriate locations in `/var/www`. To do so the server where the manager is running must be able to [ssh](https://en.wikipedia.org/wiki/Secure_Shell) into the server where the website is served from.

For local development, the meteo.report repo is most likely cloned side-by-side with meteo.report Manager, and run with `hugo server ....`; nonetheless Celery must upload the files with rsync, and to do so the `celeryworker` service must be able to ssh into your host.

### Testing pseudo radar locally

You can generate an image of the observed precipitation of the euregio region starting from a netcdf, we've placed an example in this repository: `test/radar_test.nc`

To test this procedure is needed to specify the location where the output image will be saved in `/.envs/.local/.django` in the variable `TINIA_WEBSITE_STATIC`. Remember that from that folder the image will be placed in `/var/images/radar`.

Prepare a `pseudoradar_bulletin` by an insert to the db: a simple way is to use [Adminer](https://www.adminer.org/): [http://localhost:8090](http://localhost:8090/?pgsql=postgres&username=CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV&db=tinia_manager&ns=public&select=pseudoradar_bulletins). Save the id of the bulletin for later.

Be sure to have the docker in execution with `docker-compose -f local.yml`, then enter in the celeryworker shell with:

```
docker compose -f local.yml run --rm celeryworker python manage.py shell
```

Now run (Where 1 is the ID of the bulletin inserted before):

```
from tinia_manager.tinia.tasks import post_process_radar
post_process_radar(1, "test/radar_test.nc")
```


#### Define destinations

The remote destination where the meteo.report files are copied is defined with the variable `TINIA_WEBSITE_STATIC`, for example:

- for the local development: `username@host.docker.internal:src/tinia-website/static` (assuming `username` is your user on your host, and the meteo.report website repository is cloned at `/home/username/src/tinia-website`)
- for the production site: `tinia@meteo.report:/var/www/html`
- for the sandbox: `tinia@tinia.simevo.com:/var/www/tinia_simevo`

The base url for accessing the manager UI is defined with the variable `SERVER_ADDRESS`, for example:

- for the local development: http://localhost:8000
- for the production site: https://manager.meteo.report
- for the sandbox: https://tinia-manager.simevo.com 

The base url for the API calls is defined with the variable `DJANGO_ADDRESS`, for example:

- for the local development: http://django:8000
- for the production site: https://manager.meteo.report
- for the sandbox: https://tinia-manager.simevo.com

**NOTE**: in order to make a change in `.envs` effective you need to turn docker off and on (`docker-compose ...down` then `... up`).

#### Celery ssh keys

Since ultimately the postprocessing is run by the `celeryworker` service, it needs ssh keys authorized to access your host, so locally you have to:

- install `openshh-server`:

    ``` bash
    sudo apt install openssh-server 
    ```

- make a directory called `ssh_celeryworker` with new ssh keys, add the public one to your `authorized_keys` and mark your host signature as known, with these commands:

    ``` bash
    cd ssh_celeryworker
    ssh-keygen -t ed25519 -f ./id_ed25519  # set empty passphrase
    touch ~/.ssh/authorized_keys
    cat id_ed25519.pub >> ~/.ssh/authorized_keys
    ssh-keyscan host.docker.internal > known_hosts
    ```

### Sentry

Sentry is an error logging aggregator service. You can sign up for a free account at <https://sentry.io/signup/?code=cookiecutter> or download and host it yourself.
The system is set up with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.

## Deployment

The following details how to deploy this application.

### Docker

See detailed [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).

## Updating JavaScript dependencies

JS and CSS dependencies are managed with `yarnpkg` and the files are put in place with `make` then checked into the repo.

In this way the files are vendored and the repo is self-sufficient, but it's easy to upgrade them and add new dependencies.

To **upgrade** the dependencies:

    yarnpkg upgrade

then reinstall them:

    make

To **add a new dependency**, install it with `yarnpkg add ...` or `yarnpkg add -D ...`, update the `Makefile` to put in place the files you need and `git add ...` them.

Dont't forget to commit your changes!
