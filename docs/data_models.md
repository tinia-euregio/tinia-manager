# meteo.report - Data Models

[[_TOC_]]

## Introduction

The data required to create the weather reports are:

 - `reference` data: may be common among different products and are seldom changed

 - data that arrive **automatically** at given times via web-hooks:

   - `real time` data: collected by sensors and weather stations
   - `forecast` data: calculated by mathematical weather models

 - `product` data proper: entered and checked manually by the specialists using the **meteo.report Manager** web application

The data are persisted in a database developed in the PostgreSQL relational environment, and sent to the meteo.report public-facing website (in short: meteo.report) in [JSON](https://www.json.org/json-en.html) format, the _lingua franca_ for front-end development. 

The structure of the **relational database** must:

 - handle a relatively large quantity of automatic data: numerical values and classes (sky conditions e.g. weather icons)
 - handle a reduced number of manually entered text data, in the three languages supported by meteo.report (English, German and Italian)
 - be optimized for the most common queries
 - store separately the data coming from the different weather services
 - allow restoring the data for a single weather service to a previous state

## Criteria for data retention

The main object we persist is a **bulletin**.

A bulletin is just a way for a **certain person** (the author) or **external application** (a script or separate project) to freeze at a **certain time (sending time)** (`last_update` field) the weather situation valid from a **given moment** (`start`) until **another moment** (`end`).

The weather situation associated with a bulletin is attached to the bulletin as related records in separate **data tables**; there are separate tables for textual data (`TEXTS`) and for numerical data (`DATA`).

Textual and numerical data come from different sources and therefore have different restore requirements. The former is interactively entered by the users, so it is possible to make errors and it must be possible to restore the previous state. The latter is updloaded programmatively; in case of error the upload can simply be repeated so there is no need to provide for restoring the previous state.

## E-R diagram

![ER meteo.report](./entity_relationship.png "Entity-Relationship diagram")

## `Reference` data

### Registry of time layouts (`TIME_LAYOUTS`)

Contains information about the reference time layouts for the data validity.

Columns:

- `id`: Identification code of the time layout (primary key)
- `start_day_offset`: Start offset w.r.t. the date of the bulletin reference timestamp
- `end_day_offset`: End offset w.r.t. the date of the bulletin reference timestamp
- `start_time`: Start time of the time layout
- `end_time`: End time of the time layout
- `minutes`: Duration in minutes.

Although the system can represent time ranges up to 33 days in the future or in the past, for practical reasons currently we only have time layouts with the following durations:

- 4320 minutes (72 hours = 3 days, used for mountain weather textual bulletins): `4 + 1 + 4 = 9` layouts from 4 days in the past to 4 days in the future
- 1440 minutes (24 hours = 1 day, used for textual bulletins): `6 + 1 + 5 = 12` layouts from 6 days in the past to 5 days in the future
- 180 minutes (3 hours, used for data bulletins that carry forecasts): `6 * 8 = 48` layouts from today to 5 days in the future
- and 30 minutes (half hour, used for data bulletins that carry observations): `(6 + 1) x 48 = 336` layouts from 6 days in the past to today.

The ids are self-explanatory:

1. Each set of time layouts with equal duration ("homogeneous") is centered around an intuitive reference: the `X`-minutes range starting from `bulletin.reference` has the code `X * 100000`
2. The offset w.r.t. `bulletin.reference` in minutes is `id - X * 100000`
3. Sets of homogeneous ranges are contiguous so that to loop though consecutive time layouts with `X`-minutes duration we can use `X` as constant step.

The system can not represent time ranges shorter than one minute or longer than 8 days, or time offsets greater than 33 days in the future or in the past.

Exemplificative data:

| id        | start_day_offset | end_day_offset | start_time | end_time | description                                      |
|-----------|------------------|----------------|------------|----------|--------------------------------------------------|
| 432002880 | 2                | 4              | 00:00:00   | 23:59:59 | the 3 days starting from the day after tomorrow  |
| 143998560 | -1               | -1             | 00:00:00   | 23:59:59 | the previous 24 hours                            |
| 18000000  | 0                | 0              | 00:00:00   | 02:59:59 | the next 3 hours                                 |
| 144000000 | 0                | 0              | 00:00:00   | 23:59:59 | the next 24 hours                                |
| 2991360   | -6               | -6             | 00:00:00   | 00:29:59 | the half hour that started 6 days ago            |
| 2999970   | -1               | -1             | 23:30:00   | 23:59:59 | the previous half hour                           |
| 3000000   | 0                | 0              | 00:00:00   | 00:29:59 | the next half hour                               |
| 3000660   | 0                | 0              | 11:00:00   | 11:29:59 | the half hour starting from 11 hours from now    |

A time layout is related to the reference timestamp the bulletin (`bulletins.reference`) and to the range of validity timestamps (`start` and `end`) of a certain text or data record, by means of the following expressions (Python pseudo-code):

    start = bulletins.reference + timedelta(days=start_day_offset) + start_time
    end = bulletins.reference + timedelta(days=end_day_offset) + end_time

Not all reference timestamps are allowed:

- textual bulletins **must** have a reference timestamp which is at midnight UTC
- data bulletins that carry 24-hours forecasts **must** have a reference timestamp which is at midnight UTC
- data bulletins that carry 3-hours forecasts **must** have reference timestamps at 00:00, 03:00, 06:00 ... 21:00 UTC
- data bulletins that carry observations (i.e. half-hour data) **must** have reference timestamps at 00:00, 00:30, 01:00, 01:30 ... 23:30 UTC

(these constraints are required to make sure different text and data records can be aligned).

Sample of application of a few of the exemplificative time layouts (all times UTC) assuming today = 2022-07-20:

| time_layouts.id | bulletins.reference | start               | end                 | description                                                |
|-----------------|---------------------|---------------------|---------------------|------------------------------------------------------------|
| 432002880       | 2022-07-20 00:00:00 | 2022-07-22 00:00:00 | 2022-07-24 23:59:59 | the 3 days starting from the day after tomorrow            |
| 143998560       | 2022-07-20 00:00:00 | 2022-07-19 00:00:00 | 2022-07-19 23:59:59 | yesterday                                                  |
| 144000000       | 2022-07-20 00:00:00 | 2022-07-20 00:00:00 | 2022-07-20 23:59:59 | today                                                      |
| 18000000        | 2022-07-20 15:00:00 | 2022-07-20 15:00:00 | 2022-07-20 17:59:59 | the 3 hours starting from today at 15:00 UTC               |
| 2999970         | 2022-07-20 11:30:00 | 2022-07-20 11:00:00 | 2022-07-20 11:29:59 | the half hour that ends today at 11:30 UTC                 |
| 3000000         | 2022-07-20 11:00:00 | 2022-07-20 11:00:00 | 2022-07-20 11:29:59 | the half hour that begins today at 11:00 UTC               |
| 3000660         | 2022-07-20 00:00:00 | 2022-07-20 11:00:00 | 2022-07-20 11:29:59 | the half hour that begins 11 hours from today at 00:00 UTC |

**NOTE**: the same `start` and `end` values can be described in different ways; actually the last three examples yield the same data validity timestamps.

The currently valid time layouts can be downloaded from the manager itself: [https://manager.meteo.report/api/time_layouts/](https://manager.meteo.report/api/time_layouts/).

### Registry of data types (`DATA_TYPES`)

Contains the types of JSON data that are received from the different web-hooks.

Columns:

- `id`: Identification code of the type (primary key)
- `description`: Description of the data type
- `schema`: [JSON schema](https://json-schema.org) to validate the received JSON, in [JSONB](https://www.postgresql.org/docs/current/datatype-json.html) format

The list of currently valid data types can be downloaded from the manager itself: [https://manager.meteo.report/api/schemas/](https://manager.meteo.report/api/schemas/), additionally each JSON schema can downloaded from a separate url, for example: [https://manager.meteo.report/api/schemas/forecast_message_schema/](https://manager.meteo.report/api/schemas/forecast_message_schema/).

### Registry of sky conditions (`SKY_CONDITIONS`)

Contains the list of sky condition icon files (see `tinia-website/static/images/icons`), with night variant if available and the icon names in all three supported languages, as required for accessibility.

Columns:

- `id`: Identification code of the sky condition, 1 character (primary key)
- `icon_day`: File name of the daytime icon (with sun if present) or daytime/nighttime invariant icon
- `icon_night`: File name of the nighttime icon (with moon) or `NULL` if the icon is the same as the day icon
- `name_deu`: Name of the icon in German
- `name_eng`: Name of the icon in English
- `name_ita`: Name of the icon in Italian
- `svg_day`: Svg markup for the daytime icon (with sun if present) or daytime/nighttime invariant icon
- `svg_night` Svg markup for the nighttime icon (with moon) or `NULL` if the icon is the same as the day icon

**NOTE**: the `svg_day` and `svg_night` fields must have no `id` attributes, and class names must be unique, i.e. `cls-xy-...` where `x = a, b, c ...` and `y = d, n` for day, night.

The currently valid sky conditions can be downloaded from the manager itself: [https://manager.meteo.report/api/sky_conditions/](https://manager.meteo.report/api/sky_conditions/).

### Venue types (`VENUE_TYPES`)

Used to differentiate between types of venues.

Columns:

- `id`: Identification code of the venue type (primary key)
- `description`: Explicative text

Exemplificative data:

| id | description     |
|----|-----------------|
| 0  | Region          |
| 1  | Mountain region |
| 2  | Town            |

### Registry of the venues (`VENUES`)

Contains the list of locations e.g. points on the earth, cities or political/non-political geographical areas.

Columns:

- `id`: Identification code of the venue (primary key, uuid)
- `elevation`: Elevation of the venue in meters (only for towns)
- `lat`: Latitude in WGS84 reference (only for towns)
- `lon`: Longitude in WGS84 reference (only for towns)
- `name_deu`: Name of the venue in German
- `name_eng`: Name of the venue in English
- `name_ita`: Name of the venue in Italian
- `weight`: used to show only important towns on maps when there is limited space; higher values indicate higher importance
- `id_venue_type`: Identification code of the venue type (foreign key in the table `VENUE_TYPES`)
- `id_region`: Identification code of the region for the venue (internal foreign key)

The names are differentiated for the three languages supported by meteo.report, identified by their Alpha-3 code according to [ISO 639-3](https://en.wikipedia.org/wiki/ISO_639-3).

Exemplificative data:

| id                                   | elevation | lat        | lon        | name_deu          | name_eng                 | name_ita                 | id_venue_type | id_region                            |
|--------------------------------------|-----------|------------|------------|-------------------|--------------------------|--------------------------|---------------|--------------------------------------|
| 1aa901c4-2315-4b71-a871-a0a506f90ff1 | null      |            |            | Euregio           | Euregio                  | Euregio                  | 0             | null                                 |
| aadb73d3-bab3-484f-accf-01e19f7fcc04 | null      |            |            | Tirol             | Tyrol                    | Tirolo                   | 0             | null                                 |
| 7d2d63a2-5f2c-4b53-841f-22c50ea03768 | null      |            |            | Südtirol          | South Tyrol              | Alto Adige               | 0             | null                                 |
| fba93146-7192-4190-adab-605435fdeea1 | null      |            |            | Trentino          | Trentino                 | Trentino                 | 0             | null                                 |
| 9f5c2ad3-eafd-4c75-95cd-a3fac22f4d4b | null      |            |            | Berge im Tirol    | Mountains in Tyrol       | Montagne del Tirolo      | 1             | aadb73d3-bab3-484f-accf-01e19f7fcc04 |
| 1ce7b98d-6431-4540-8be8-0c9a710bc61b | null      |            |            | Berge im Südtirol | Mountains in South Tyrol | Montagne dell'Alto Adige | 1             | 7d2d63a2-5f2c-4b53-841f-22c50ea03768 |
| d1e9a3da-4ed5-4ec7-87e6-bb05b43d0f49 | null      |            |            | Berge im Trentino | Mountains in Trentino    | Montagne del Trentino    | 1             | fba93146-7192-4190-adab-605435fdeea1 |
| c4efef40-bb6f-4cd3-a982-4e7579da701e | 574       | 47.267222  | 11.392778  | Innsbruck         | Innsbruck                | Innsbruck                | 2             | aadb73d3-bab3-484f-accf-01e19f7fcc04 |
| 7295de91-819c-4109-a790-3b7cf84aa79b | 262       | 46.49926   | 11.35661   | Bozen             | Bolzano                  | Bolzano                  | 2             | 7d2d63a2-5f2c-4b53-841f-22c50ea03768 |
| 88c9882b-33f3-4d83-b2ed-cd946b774673 | 194       | 46.066667  | 11.116667  | Trient            | Trento                   | Trento                   | 2             | fba93146-7192-4190-adab-605435fdeea1 |

The currently valid venues can be downloaded from the manager itself: [https://manager.meteo.report/api/venues/](https://manager.meteo.report/api/venues/).

## `Product` data

All bulletin tables have a set of common fields:

- `id`: Auto-incremented integer that identifies the bulletin (primary key)
- `reference`: Reference timestamp the bulletin
- `start`: Timestamp of validity start of the bulletin
- `end`: Timestamp of validity end of the bulletin
- `last_update`: Timestamp of the last update
- `author`: Author of the last update
- `version`: Integer incremented with each save operation of the draft
- `draft`: Bulletin status, `TRUE` = draft, `FALSE` = sent
- `id_venue`: Identification code of the venue (foreign key in the table `VENUE`; only venues of types `0` = `Region` have an associated bulletin)

Once a bulletin is sent (draft status = `FALSE`), it can not be modified, but it can be cloned in a new draft.

### Text bulletin main table (`TEXT_BULLETINS`)

Contains the metadata and core information for a **text bulletin**, i.e. a bulletin with associated records in the `TEXT` table.

For a given validity interval (`start - end`) there are normally four **active** textual bulletins, one for each weather service Tirol, Südtirol and Trentino) and one for the whole Euregio (identified by the `id_venue`), each can be sent at a different time.

If the bulletin for one or more weather services is missing, the system will use one from a previous day, in which case:

- the values with validity more forward in time will be missing
- a warning message will appear in the corresponding fields of the meteo.report (e.g. "_WARNING: the text forecast for the weather in the mountains of Tyrol has not been updated since day X at HH:MM_".
- if the data does not arrive by a predefined time (e.g. 12:00), a "missing data" e-mail notification can be sent to the person in charge of the weather service.

Multiple bulletins can be sent for a given validity interval (repeated sending in case of corrections).

For each venue the active bulletin is always the non-draft one with the most recent sending timestamp (`SELECT * FROM bulletins WHERE NOT draft ORDER BY last_update DESC LIMIT 1`).

To restore a bulletin to a previous state you can "touch" the previously sent bulletin (`UPDATE bulletins SET last_update = NOW() WHERE ...`).

Exemplificative data:

| id | id_venue     | last_update      | start            | end              | draft  | author | version |
|----|--------------|------------------|------------------|------------------|--------|--------|---------|
| 0  | 0 (Tirol)    | 2022-03-05 10:30 | 2022-03-05 00:00 | 2022-03-07 23:59 | FALSE  | alice  | 5       |
| 1  | 1 (Südtirol) | 2022-03-05 11:25 | 2022-03-05 00:00 | 2022-03-07 23:59 | FALSE  | bob    | 10      |
| 2  | 2 (Trentino) | 2022-03-05 12:10 | 2022-03-05 00:00 | 2022-03-07 23:59 | FALSE  | carla  | 4       |
| 3  | 1 (Südtirol) | 2022-03-05 11:25 | 2022-03-05 00:00 | 2022-03-07 23:59 | FALSE  | bob    | 4       |
| 4  | 1 (Südtirol) | 2022-03-05 15:10 | 2022-03-05 00:00 | 2022-03-07 23:59 | FALSE  | bob    | 2       |
| 5  | 1 (Südtirol) | 2022-03-05 16:13 | 2022-03-05 00:00 | 2022-03-07 23:59 | TRUE   | bob    | 1       |

In this example:

- The weather services of Tyrol and Trentino have sent their data only once.
- Südtirol has sent its data three times and is working on a fourth submission (still in draft)
- The data shown on the meteo.report website will be the combination of the data from bulletins 0, 2 and 4.

### Textual data table (`TEXTS`)

Contains the dynamic, textual part of the bulletins.

Columns:

- `id`: Auto-incremented integer that identifies the text value (primary key)
- `value_deu`: Text value in German
- `value_eng`: Text value in English
- `value_ita`: Text value in Italian
- `id_text_bulletin`: Identification code of the bulletin (foreign key in the table `BULLETINS`)
- `id_time_layout`: Identification code of the time layout (foreign key in the table `TIME_LAYOUTS`; only time layouts with `seconds` = 86400 have an associated entry in the `TEXTS` table)
- `id_venue`: Identification code of the venue (foreign key in the table `VENUE`; only venues of types `0` = `Region` and `1` = `Mountain region` have an associated entry in the `TEXTS` table)

The values are differentiated for the three languages supported by meteo.report, identified by their Alpha-3 code according to [ISO 639-3](https://en.wikipedia.org/wiki/ISO_639-3).

### Data bulletin main table (`DATA_BULLETINS`)

Contains the metadata and core information for a **data bulletin**, i.e. a bulletin with associated records in the `DATA` table.

### Numerical data table (`DATA`)

Contains the data that arrive **automatically** at given intervals, for which the restore functionality is not required.

Since we are dealing with large amounts of data whose structure is fluid, we store them in the database in a **no-SQL** form, e.g. in [JSON](https://www.json.org/json-en.html) format.

Columns:

- `id`: Auto-incremented integer that identifies the data (primary key)
- `value`: data in [JSONB](https://www.postgresql.org/docs/current/datatype-json.html) format
- `id_data_bulletin`: Identification code of the bulletin (foreign key in the table `DATA_BULLETINS`)
- `id_data_type`: Identification code of the data type (foreign key in the table `DATA_TYPES`)
- `id_time_layout`: Identification code of the time layout (foreign key in the table `TIME_LAYOUTS`)
- `id_venue`: Identification code of the venue (foreign key in the table `VENUE`; only venues of type `2` = `Town` have an associated entry in the `DATA` table)

## How to browse the database

Since the following stance is present in the configuration of the `postgres` service in local development mode (see `local.yml` under the `postgres` key):

    ports:
      - "5433:5432"

docker-compose establishes a connection between port 5433 of the host, and the default postgres port in the container (5432) which would otherwise only be accessible from the internal network of the docker services.

For this reason from the host you can enter the DB of the container typing from the terminal:

    PGPASSWORD=IU2Cxkck4VF9mibp7EBN4drslft7PG0ApYxbW7RPVbCfBSJRqulezsyLUE0yu5Qg psql -p 5433 -h localhost -U CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV tinia_manager

**pgModeler**

Install pgModeler with:

    sudo apt install pgmodeler

launch it and connect to the DB with:

 - host = localhost
 - port = 5433
 - user = CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV
 - pass = IU2Cxkck4VF9mibp7EBN4drslft7PG0ApYxbW7RPVbCfBSJRqulezsyLUE0yu5Qg

**Adminer**

Since the `adminer` service is enabled in local development mode (see `local.yml`) you can browse the database at the link http://localhost:8090/?pgsql=postgres&username=CDtTJeXLnNXCMWUxUPmekDMeAgtckRFV&db=tinia_manager&ns=public) (password: `IU2Cxkck4VF9mibp7EBN4drslft7PG0ApYxbW7RPVbCfBSJRqulezsyLUE0yu5Qg`).
