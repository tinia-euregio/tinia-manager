# meteo.report Manager Deployment Guide

This document details how to deploy meteo.report Manager to production.

[[ TOC ]]

## Preprequisites

**Sandbox** and **production** installs are preferably done with [ansible](https://www.ansible.com/), minimum required version is 2.10.

Ansible must be called with the [COLLECTIONS_PATH](https://docs.ansible.com/ansible/latest/reference_appendices/config.html#collections-paths) option set the directory where the collections are located (rather than in the default location `~/.ansible`).

Install it on the control host:

- Debian 12 (bookworm): `sudo apt install ansible`

- macOS:

 - `brew install ansible`
 
 - alternate [using pip](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-macos)

Then set-up a dedicated Debian 12 (bookworm) machine as a lxc / nspawn container, VirtualBox Virtual Machine (VM), cloud-hosted Virtual Private Server (VPS) or baremetal machine; for local tests and development, an lxc container is OK.

The target host `manager.meteo.report` must the be reachable by DNS, else trick it by adding to your `/etc/hosts` something like:

```
192.168.x.y   manager.meteo.report
```

Set up passwordless ssh access for user `root` to the target, then install ansibile prerequisites:

    ssh root@manager.meteo.report apt update
    ssh root@manager.meteo.report apt install -y python3

## Install meteo.report Manager

Launch the install with:

    ANSIBLE_NOCOWS=1 ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-playbook -i ansible/hosts ansible/sandbox.yml

## Custom translations

Set up the custom translations server then deploy to the meteo.report Manager server the files:

1. `/srv/manager/secrets/fbk_ip`: contains the IP address of your custom translations server (no trailing endline!);

2. `/srv/manager/secrets/userpass`: the basic auth credentials for the custom translations server in `username:password` format (no trailing endline!).

To create these files without trailing endline, use a command similar to: `echo -n "..." > secrets/fbk_ip`.

## Galaxy dependencies

Ansible galaxy is configured with a [YAML role requirements file](https://docs.ansible.com/ansible/latest/user_guide/collections_using.html), the necessary collections have been installed with:

    ANSIBLE_NOCOWS=1 ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-galaxy install -r ansible/requirements.yml

and are checked into the repository. To force re-download from galaxy [while we wait for the ansible-galaxy update option](https://github.com/ansible/proposals/issues/23):

    rm -rf ansible/collections
    ANSIBLE_NOCOWS=1 ANSIBLE_COLLECTIONS_PATH="$PWD/ansible" ansible-galaxy install --force -r ansible/requirements.yml

## Email

Test the SMTP configuration:

    echo "My message" | mail -r noreply@manager.meteo.report -s subject john.doe@example.com

Check what exim is doing:

    systemctl status exim4
    exiwhat

Count the messages in the queue:

    exim -bpc

List outbound emails in exim4 queue:

    mailq

Force reprocessing of the quete and retry sending all messages (verbose mode):

    exim4 -qff -v

View e-mail headers for a specific message in the queue:

    exim -Mvh <message_id>

View e-mail body for a specific message in the queue:

    exim -Mvb <message_id>

Remove one or more messages from the queue:

    exim -Mrm <message_id_1> <message_id_2> <...>

Clear all frozen messages from the queue:

    mailq | awk '/frozen/{print "exim4 -Mrm "$3}' | /bin/sh
